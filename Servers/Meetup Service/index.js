var express = require("express");
var path = require("path");
var app = express();
var firebase = require("firebase");
var axios = require("axios");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var admin = require("firebase-admin");
var utils = require("utils");
const rp = require("request-promise");

//TODO: authentication - who is sending the get/post requests?
var client_id = "";
var client_secret = "";
var redirect_uris = "";

var userURL = "https://bitsplease-user-service.herokuapp.com/";
var newLoginURL = userURL + "newLogin/";
var teamURL = "https://bitsplease-team-service.herokuapp.com/";
var getUIDsURL = teamURL + "getUIDs";
const voteURL = "https://bitsplease-voting-service.herokuapp.com/";
var getVotesURL = voteURL + "getVotes";

const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

var clientJSON = {};

// Load client secrets from a local file.

fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Calendar API.
  
    clientJSON = JSON.parse(content);
    console.log(clientJSON);

    client_secret = clientJSON.installed.client_secret;
    client_id = clientJSON.installed.client_id;
    redirect_uris = clientJSON.installed.redirect_uris;
});

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */



/*==============================================
 * Initialization
 *==============================================*/
// Initialize Firebase
firebase.initializeApp({
    apiKey: "AIzaSyAH3EJzjsZ7RkBfgKlFsoaSFxySMtncm8w",
    authDomain: "cpen321-meetups.firebaseapp.com",
    databaseURL: "https://cpen321-meetups.firebaseio.com",
    projectId: "cpen321-meetups",
    storageBucket: "cpen321-meetups.appspot.com",
    messagingSenderId: "572530108268"
});

// Get a reference to the firestore service
var db = firebase.firestore();


app.set("port", (process.env.PORT || 5000));

app.use(express.json());
app.use(express.urlencoded());


app.use(function(req,res,next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type,Accept");
	next();
});

// allows access from local host
app.use("/", express.static(path.join(__dirname, "..")));




/*==============================================
 * Assorted helper functions
 *==============================================*/
var userURL = "https://bitsplease-user-service.herokuapp.com/";
var getCalTokenURL = userURL + "getCalToken";

function mergeFreeBusy(freeBusys){
    var newCal = [];
    var numCals = freeBusys.length;
    var inds = [];
    
    for(i = 0; i < numCals; i++){
        inds.push(0);
    }
    
    while(true){
        // find the closest time at the head of the events
        var calEvent = {};
        var nearestStart = {};
        var nearestInd = -1;
        
        for(var i = 0; i < numCals; i++){
            var cal = freeBusys[i];
            
            if(inds[i] < cal.length){
                var headEvent = cal[inds[i]];
                
                var startDateTime = new Date(headEvent.start);
                
                if(nearestInd === -1 || nearestStart.getTime() > startDateTime.getTime()){
                    nearestStart = startDateTime;
                    calEvent = headEvent;
                    nearestInd = i;
                }
            }
        }
        
        if(nearestInd === -1){
            break;
        }
        else{
            startDate = new Date(calEvent.start);
            endDate = new Date(calEvent.end);
            
            newCalEvent = {
                "start":addMinutes(startDate, -480), 
                "end":addMinutes(endDate, -480)
                };
            
            newCal.push(newCalEvent);
            inds[nearestInd] += 1;
        }
    }
    
    console.log(newCal);
    return newCal;
}

function getFreeBusyProm(auth, resource) {
    const calendar = google.calendar({version: 'v3', auth});
    
    return new Promise(function (resolve, reject) {
        calendar.freebusy.query({
            auth, 
            resource
        }, function (err, res) {
            if (err){
                console.log(err);
                reject(err)
            };
            resolve(res);
        });
    });
}

function getAllFreeBusy(startDate, endDate, cb, calTokens){
    async function getCals(){
        var count = calTokens.length;
        var cals = [];
        console.log("trying to get cals");
        for(var i = 0; i < count; i++){
            
            const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
            oAuth2Client.setCredentials(JSON.parse(calTokens[i]));
            
            var resource = {
                timeMin: (startDate).toISOString(),
                timeMax: (endDate).toISOString(),
                items:[{id: 'primary'}]
            };
            
            res = await getFreeBusyProm(oAuth2Client, resource);
            
            cals.push(res.data.calendars.primary.busy);
            
            console.log("recieved a cal");
        }
        
        console.log("recieved calendars");
        var mergedCal = mergeFreeBusy(cals);
        
        return cb(mergedCal);
    }
    
    getCals().catch(err => console.log) // Don't forget to catch errors
    
}
 

function getAllTokens(cb, userIDs,){
    async function getTokens(){
        var count = userIDs.length;
        var tokens = [];
        
        for(var i = 0; i < count; i++){
            var url = getCalTokenURL + "?UID=" + userIDs[i];
            var response = await axios.get((getCalTokenURL + "?UID=" + userIDs[i]));
            
            if(typeof response.data.calToken !== "undefined")
                tokens.push(response.data.calToken);
        }
        
        console.log("recieved user tokens");
        return cb(tokens);
    }
    
    getTokens().catch(err => console.log) // Don't forget to catch errors
    
}

function findConflicts(cal, start, end){
    var length = cal.length;
    var startDate = new Date(start);
    var endDate = new Date(end);
    
    var numConflicts = 0;
    
    for(var i = 0; i < length; i++){
        calEvent = cal[i];
        
        calStartDate = new Date(calEvent.start);
        calEndDate = new Date(calEvent.end);
        
        if(calStartDate.getTime() > endDate.getTime()){
            break;
        }
        else if(calStartDate.getTime() > startDate.getTime() && calStartDate.getTime() < endDate.getTime()){
            numConflicts += 1;
        }
        else if(calEndDate.getTime() > startDate.getTime() && calEndDate.getTime() < endDate.getTime()){
            numConflicts += 1;
        }
        else if(calStartDate.getTime() < startDate.getTime() && calEndDate.getTime() > endDate.getTime()){
            numConflicts += 1;
        }
    }
    
    console.log("conflicts: " + numConflicts);
    
}

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}

function checkIfReplace(bestTimes, newNumConflicts, newStartTime){
    var worstElConflicts = -1;
    var worstElInd = -1;
    
    for(var i = 0; i < bestTimes.length; i++){
        
        oldNumConflicts = bestTimes[i][0];
        
        if(worstElInd === -1 || worstElConflicts < oldNumConflicts){
            worstElInd = i;
            worstElConflicts = oldNumConflicts;
        }
        else if(worstElConflicts === oldNumConflicts){
            if(isTimeBetter(bestTimes[worstElInd][1], bestTimes[i][1])){
                worstElInd = i;
                worstElConflicts = oldNumConflicts;
            }
        }
    }
    
    if(worstElConflicts > newNumConflicts){
        return worstElInd;
    }
    else if(worstElConflicts === newNumConflicts){
        if(isTimeBetter(newStartTime, bestTimes[worstElInd][1])){
            return worstElInd;
        }
    }
    else{
        return -1;
    }
}

function isTimeBetter(date1, date2){
    var hour1 = date1.getUTCHours();
    var hour2 = date2.getUTCHours();
    
    var offFromNoon1 = hour1 - 12; 
    if(hour1 < 12){
        offFromNoon1 = 12 - hour1;
    }
    
    var offFromNoon2 = hour2 - 12; 
    if(hour2 < 12){
        offFromNoon2 = 12 - hour2;
    }
    
    if(offFromNoon1 < offFromNoon2){
        return true;
    }
    // gives a little diversity in generated times
    else if(offFromNoon1 === offFromNoon2){
        return (Math.random() >= 0.5)
    }
    // second time isn't THAT bad 
    else if(offFromNoon2 <= 2){
        return (Math.random() >= 0.3)
    }
    // second time isn't great, but give it a chance
    else if(offFromNoon2 <= 4){
        return (Math.random() >= 0.1)
    }
    
    return false;
}

function countConflicts(cal, startDate, endDate){
    
    var length = cal.length;
    
    var numConflicts = 0;
    
    for(var i = 0; i < length; i++){
        calEvent = cal[i];
        
        calStartDate = new Date(calEvent.start);
        calEndDate = new Date(calEvent.end);
        
        if(calStartDate.getTime() > endDate.getTime()){
            break;
        }
        else if(calStartDate.getTime() >= startDate.getTime() && calStartDate.getTime() < endDate.getTime()){
            numConflicts += 1;
        }
        else if(calEndDate.getTime() > startDate.getTime() && calEndDate.getTime() <= endDate.getTime()){
            numConflicts += 1;
        }
        else if(calStartDate.getTime() <= startDate.getTime() && calEndDate.getTime() >= endDate.getTime()){
            numConflicts += 1;
        }
    }
    
    return numConflicts;
    
}

function findLeastConflicts(rangeStart, rangeEnd, numMinutes, numTimes, cb, cal){
    var bestTimes = [];
    
    var startTime = rangeStart;
    var endTime = addMinutes(startTime, numMinutes);
    
    while(endTime.getTime() < rangeEnd.getTime()){
        
        var numConflicts = countConflicts(cal, startTime, endTime);
        
        if(bestTimes.length < numTimes){
            bestTimes.push([numConflicts, startTime]);
        }
        else{
            var ind = checkIfReplace(bestTimes, numConflicts, startTime);
            if(ind >= 0){
                bestTimes[ind] = [numConflicts, startTime];
            }
        }
        
        startTime = addMinutes(startTime, 15);
        endTime = addMinutes(startTime, numMinutes);
    }
    
    console.log(bestTimes);
    
    var justTimeStrings = [];
    
    for(var i = 0; i < bestTimes.length; i++){
        var timeStr = bestTimes[i][1].toUTCString();
        timeStr = timeStr.substring(0, timeStr.length - 7);
        
        justTimeStrings.push(timeStr);
    }
    
    justTimeStrings.sort();
    
    cb(justTimeStrings);
}
 
// test code which does a post request within the same server
app.get("/testNewMeetUp", function(myRequest, myResponse) {
    
    const url="https://bitsplease-meetup-service.herokuapp.com/";
    
    axios.post(url + "newMeetup/", {
        meetupname: "my test name",
        startdate: "12/10/2018",
        starttime: "12:00",
        enddate: "12/12/2018",
        endtime: "21:00",
        attributes: [],
        TID: "00testingteam",
        pollDuration: 1,
        meetingDuration: 120
        })
        .then(function (response) {
            // post request should have been successful
            myResponse.status = response.status;
            
            setTimeout(function(){
                axios.get(voteURL + "getVotes?PID=" + response.data.PID)
                    .then(function (response1) {
                        console.log("RESPONSE 1:");
                        console.log(response1.data);
                    }).catch(function (error) {
                        console.log("error getting votes: " + error.response.data);
                    });
            }, 50);
            
            setTimeout(function(){
                axios.put(voteURL + "vote?PID=" + response.data.PID, {
                    UID: "this is a userID",
                    vote: 0
                    })
                    .then(function (response1) {
                        console.log("VOTE RESPONSE 1:");
                        console.log(response1.data);
                    }).catch(function (error) {
                        console.log("error voting: " + error.response.data);
                    });
            }, 100);
            
            setTimeout(function(){
                axios.put(voteURL + "vote?PID=" + response.data.PID, {
                    UID: "this is also a userID",
                    vote: 2
                    })
                    .then(function (response2) {
                        console.log("VOTE RESPONSE 2:");
                        console.log(response2.data);
                    }).catch(function (error) {
                        console.log("error voting: " + error.response.data);
                    });
            }, 150);
            
            setTimeout(function(){
                axios.get(voteURL + "getVotes?PID=" + response.data.PID)
                    .then(function (response2) {
                        console.log("RESPONSE 2:");
                        console.log(response2.data);
                    }).catch(function (error) {
                        console.log("error getting votes: " + error.response.data);
                    });
            }, 200);
            
            myResponse.json({"response":response.data});
            myResponse.end();
        }).catch(function (error) {
            // Human readable description of error
            var myErrorMessage = error.response.status + " Error: occured when attempting to post data to " + url;
            utils.returnError(myResponse, myErrorMessage, error.response.status);
        });
    
});

function addOneEvent(auth, event) {
    console.log("before cal");
    const calendar = google.calendar({version: 'v3', auth});
    
    console.log("after cal");
    calendar.events.insert({
      auth: auth,
      calendarId: 'primary',
      resource: event,
    }, function(err, event) {
      if (err) {
        console.log('There was an error contacting the Calendar service: ' + err);
        return;
      }
      console.log('Event created: %s', event.htmlLink);
    });
}

function addEvents(highestVoteTime, meetingDuration, teamDisplayName, calTokens){
    for(var i = 0; i < calTokens.length; i++){
        console.log("add event of length " + meetingDuration + " at " + highestVoteTime + " for team " + teamDisplayName);
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        oAuth2Client.setCredentials(JSON.parse(calTokens[i]));
        
        var event = {
            'summary': 'Meeting with team ' + teamDisplayName,
            'description': 'This meeting was scheduled by Bits Please',
            'start': {"dateTime":addMinutes(highestVoteTime, (8*60))},
            'end': {"dateTime":addMinutes(highestVoteTime, meetingDuration + (8*60))},
            'reminders': {
                'useDefault': true
                }
        };
        
        console.log("here");
        addOneEvent(oAuth2Client, event);
    }
    
    console.log("done!");
}

function handleClosedPoll(pollID, teamID, meetupName, meetingDuration){
    console.log("handling closed poll");
    
    // fetch userIDs of team
    axios.get(getUIDsURL + "?TID=" + teamID) // TODO: change after talking with Cindy
        .then(function (response1) {
            var userIDs = response1.data.UIDs;
            
            console.log("recieved user IDs");
            
            axios.get(getVotesURL + "?PID=" + pollID) // TODO: change after talking with Cindy
                .then(function (response2) {
                    var votes = response2.data.votes;
                    
                    console.log("recieved votes");
                    
                    var highestVoteCount = 0;
                    var highestVoteInd = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVote = votes[i];
                        
                        console.log(optionVote)
                        
                        if(optionVote[1] > highestVoteCount){
                            highestVoteCount = optionVote[1];
                            highestVoteTime = new Date(optionVote[0]);
                        }
                    }
                    
                    console.log(highestVoteTime);
                    console.log(userIDs);
                    
                    var myAddEvents = addEvents.bind(null, highestVoteTime, meetingDuration, teamID);
                    getAllTokens(myAddEvents, userIDs);
                    
                }).catch(function (error) {
                    console.log(error);
            });                
        }).catch(function (error) {
            console.log(error);
    });
    
}

function newPollRequest(teamID, meetupID, meetupName, pollDuration, meetingDuration, myResponse, options){
    axios.post(voteURL + "newPoll/", {
        "TID": teamID,
        "MID": meetupID,
        meetupName,
        options
        })
        .then(function (response) {
            // post request should have been successful
            var pollID = response.data.PID;
            console.log("here1");
            // automatically close the poll after specified duration
            setTimeout(function(){
                console.log("closing poll");
                axios.put(voteURL + "closePoll?PID=" + pollID)
                .then(function(response2){
                    console.log("here2");
                    handleClosedPoll(pollID, teamID, meetupName, meetingDuration);
                });
            }, 1000 * pollDuration);
            
            myResponse.status = 200;
            myResponse.json({"MID":meetupID, "PID":pollID});
            myResponse.end();
                    
        }).catch(function (error) {
            // Human readable description of error
            var myErrorMessage = error.response.status + " Error: occured when attempting to post data to " + voteURL + "newPoll";
            utils.returnError(myResponse, myErrorMessage, error.response.status);
    });
}    
    
function createNewTimePoll( userIDs, 
                            teamID, 
                            meetupID, 
                            meetupName, 
                            pollDuration, 
                            meetingDuration, 
                            startDateTime, 
                            endDateTime, 
                            myResponse){
    
    // actually send the request
    var myNewPollRequest = newPollRequest.bind(null, teamID, meetupID, meetupName, pollDuration, meetingDuration, myResponse);
    
    /*
     * Figure out the options - 
     * done by getting all tokens, then getting all calendars and finding conflicts.
     */
    var myFindLeastConflicts = findLeastConflicts.bind(null, startDateTime, endDateTime, meetingDuration, 4, myNewPollRequest);
    var myGetAllFreeBusy = getAllFreeBusy.bind(null, startDateTime, addMinutes(endDateTime, meetingDuration), myFindLeastConflicts);

    getAllTokens(myGetAllFreeBusy, userIDs);
}



/*==============================================
 * GET, POST, PUT requests
 *==============================================*/
 
/*
 * POST endpoint which adds a new meetup to the database, returning it's MID
 */
app.post("/newMeetup", function(myRequest, myResponse){
    
    var meetupName = myRequest.body.meetupname;
    var startDate = myRequest.body.startdate;
    var startTime = myRequest.body.starttime;
    var endDate = myRequest.body.enddate;
    var endTime = myRequest.body.endtime;
    var teamID = myRequest.body.TID;
    var pollDuration = myRequest.body.pollDuration;
    var meetingDuration = myRequest.body.meetingDuration;
    
    var options = ["option 1", "option 2", "option 3", "option 4"];
    
    if(utils.checkIfUndefined([meetupName, startDate, startTime, endDate, endTime, teamID])){
        var myErrorMessage = "400 Error: Expected request to contain all attributes of a meetup. One or more missing.";
        utils.returnError(myResponse, myErrorMessage, 400);
    } 
    if(!utils.checkIfStrings([meetupName, teamID])){
        var myErrorMessage = "400 Error: meetup name and teamID should be non-empty strings";
        utils.returnError(myResponse, myErrorMessage, 400);
    } 
    else{
        // allows no location attributes, initializes to empty array
        if(typeof locAttributes === "undefined"){
            locAttributes = [];
        }
        // allows no durtation, initializes to 10 minutes
        if(typeof pollDuration === "undefined"){
            pollDuration = 60;
        }
        if(typeof meetingDuration === "undefined"){
            meetingDuration = 60;
        }
        
        var endDateParts = endDate.split('/');
        var endTimeParts = endTime.split(':');
        
        var startDateParts = startDate.split('/');
        var startTimeParts = startTime.split(':');
        
        if(endDateParts.length !== 3 || endTimeParts.length !== 2 || startDateParts.length !== 3 || startTimeParts.length !== 2){
            var myErrorMessage = "Did not recieve expected date format. Please contact server manager.";
            utils.returnError(myResponse, myErrorMessage, 400);
        }
        else{
        
        var endDateTime = new Date(
            endDateParts[2],        // year
            endDateParts[0] - 1,    // month
            endDateParts[1],        // day
            endTimeParts[0],        // hours
            endTimeParts[1]         // minutes
        );
        
        var startDateTime = new Date(
            startDateParts[2],        // year
            startDateParts[0] - 1,    // month
            startDateParts[1],        // day
            startTimeParts[0],        // hours
            startTimeParts[1]         // minutes
        ); 

        if(endDateTime.getTime() - startDateTime.getTime() < meetingDuration){
            var myErrorMessage = "Did not recieve a large enough window for expected meeting time";
            utils.returnError(myResponse, myErrorMessage, 400);
        }
        
        // try to add new meetup
        db.collection("Meetups").add({
            meetupName,
            startDateTime,
            endDateTime,
            meetingDuration,
            "TID":teamID,
            locAttributes
        })
        .then(function(docRef){
            // successfully added new meetup - now perform logic
            var meetupID = docRef.id;
            console.log("New meetup created with MID = " + meetupID);
            
            // fetch userIDs of team
            axios.get(getUIDsURL + "?TID=" + teamID) // TODO: change after talking with Cindy
                .then(function (response) {
                    var userIDs = response.data.UIDs;
                    
                    console.log("recieved user IDs");
                    
                    createNewTimePoll(userIDs, teamID, meetupID, meetupName, pollDuration, meetingDuration, startDateTime, endDateTime, myResponse);                   
                }).catch(function (error) {
                    utils.returnError(myResponse, myErrorMessage, error.response.status);
                    docRef.delete();
            });
        })
        .catch(function(error){
            var myErrorMessage = "500 Error: Unable to add new meetup, error returned from database: " + error;
            utils.returnError(myResponse, myErrorMessage, 500);
        });
        }
    }
});


/*
 * GET endpoint which attempts to fetch the meeting attributes belonging to the meetup 
 * with the id requested (MID)
 * 
 * Expects a query string with the name "MID" and returns an error otherwise
 */
 app.get("/getMeetup", function(myRequest, myResponse) {    
    
    // Retrieve MID from query
	var meetupID = myRequest.query["MID"];
    
    if(typeof meetupID !== "string" || meetupID.length <= 0){
        var myErrorMessage = "400 Error: Expected MID in getMeetup";
        utils.returnError(myResponse, myErrorMessage, 400);
    } 
    else{
        var docRef = db.collection("Meetups").doc(meetupID);
        
        docRef.get().then(function(doc){
            myResponse.status = 200;
            
            if(!doc.exists){
                console.log("MID " + meetupID + " is invalid, returned empty");
                
                myResponse.json({"found":false});
                
                myResponse.end();
            } else{
                console.log("MID " + meetupID + " is valid, returned meetup");
                
                var meetupJson = doc.data();
                meetupJson.found = true;
                
                myResponse.json(meetupJson);
                
                myResponse.end();
            }
        });
    }
    
});

app.listen(app.get("port"), function() {
  console.log("Node app is running at localhost:" + app.get("port"));
});

