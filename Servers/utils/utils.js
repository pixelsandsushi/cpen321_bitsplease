// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

function checkIfType(variables, type, anyVar){
        // if anyVar is true, return true if ANY variables match type.
        // else, allVar is set true, returns true if ALL variables match type.
    
        var allVar = !anyVar;
    
        if (variables.constructor !== Array){
            console.log("Recieved unexpected parameter in checkIfType: expected variables to be an Array");
            return;
        }
        if (typeof type !== "string"){
            console.log("Recieved unexpected parameter in checkIfType: expected type to be a string");
            return;
        }
        
        for(var i = 0; i < variables.length; i++){
            if(typeof variables[i] === type){
                if(anyVar){
                    return true;
                }
            }
            else{
                if(allVar){
                    return false;
                }
            }
        }
        
        // allVar should return true, anyVar should return false
        return allVar;
}

module.exports = {
    checkIfUndefined(variables){
        return checkIfType(variables, "undefined", true);
    },
    checkIfStrings(variables){
        if(checkIfType(variables, "string", false)){
            for(var i = 0; i < variables.length; i++){
                if(variables[i].length <= 0){
                    return false;
                }
            }
            
            // all strings were non-empty
            return true;
        }
        return false;
    },
    returnError(myResponse, myErrorMessage, myStatus, additionalJson){
        if(typeof additionalJson === "undefined"){
            additionalJson = {};
        }
        
        // Note: myErrorMessage should be human readable description of error
        console.log(myErrorMessage);
       
        // Respond to get request with error
        myResponse.status(myStatus); // 400 for client error, 500 for server error
        
        additionalJson.errMessage = myErrorMessage;
        additionalJson.errCode = myStatus;
        myResponse.json(additionalJson);
        
        myResponse.end();
    }
};