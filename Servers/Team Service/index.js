var im = require('istanbul-middleware');
var isCoverageEnabled = (true);

//before your code is require()-ed, hook the loader for coverage
if (isCoverageEnabled) {
    console.log('Hook loader for coverage - ensure this is not production!');
    
    console.log(__dirname);
    im.hookLoader(__dirname);
    // cover all files except under node_modules
    // see API for other options
}

var express = require("express");
var path = require("path");
var app = express();
var firebase = require("firebase");
var axios = require("axios");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var admin = require("firebase-admin");
var globMeta = require(__dirname);
var utils = require("utils");

require('./teamserver').start();