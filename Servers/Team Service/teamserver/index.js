const express = require("express");
const path = require("path");
const app = express();
const firebase = require("firebase");
const axios = require("axios");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const admin = require("firebase-admin");
const fs = require('fs');
const utils = require("utils");
const im = require('istanbul-middleware');


// Load service account secrets from a local file.
var db = {};
fs.readFile('/app/teamserver/firestoreadmin.json', (err, content) => {
    console.log(__dirname);
    
    if (err) return console.log('Error loading firestore admin file:', err);
  
    const serviceAccount = JSON.parse(content);
    
    // Initialize Firebase
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        apiKey: "AIzaSyDYD4WI1hRgBOxM5IOs3YEx0T0NtlXMuqk",
        authDomain: "cpen321-users.firebaseapp.com",
        databaseURL: "https://cpen321-users.firebaseio.com",
        projectId: "cpen321-users",
        storageBucket: "cpen321-users.appspot.com",
        messagingSenderId: "285954961097"
    });
    
    // Get a reference to the firestore service
    db = admin.firestore();
});

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */


module.exports = {
    start: function(){
        app.set("port", (process.env.PORT || 5000));

        app.use(express.json());
        app.use(express.urlencoded());
        
        // for testing only - remove if deployed
        app.use('/coverage', im.createHandler()); 


        app.use(function(req,res,next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type,Accept");
            next();
        });

        // allows access from local host
        app.use("/", express.static(path.join(__dirname, "..")));
         

        /*==============================================
         * GET, POST, PUT, DELETE request endpoints
         *==============================================*/
         
        /*
         * POST request endpoint which adds a new team to the database with one user, returning it's TID
         *
         * query requirements:  None
         * body requirements:   - UID : string
         *                      - teamDisplayName : string
         *
         * returned JSON:       - TID : string
         * 
         * returns error if: requirements are not correct type or are not present in request body, "TID" not present in JSON on error
         *                   UID is not present in the user database   
         */
        app.post("/newTeam", function(myRequest, myResponse){
            
            var userID = myRequest.body.UID;
            var displayName = myRequest.body.teamDisplayName;
            
            if(utils.checkIfUndefined([userID, displayName])){
                var myErrorMessage = "400 Error: Expected newTeam POST request to contain UID and teamDisplayName";
                utils.returnError(myResponse, myErrorMessage, 400);
            }
            else if(!utils.checkIfStrings([userID, displayName])){
                var myErrorMessage = "400 Error: Expected newTeam POST request to contain UID and teamDisplayName as non empty strings";
                utils.returnError(myResponse, myErrorMessage, 400);
            }
            else{
                // try to add new team
                db.collection("Teams").add({
                    displayName,
                    UIDs: [userID]
                })
                .then(function(teamDoc){
                    // successfully added new team
                    var teamID = teamDoc.id;
                    
                    return db.runTransaction(function(transaction){
                        userDocRef = db.collection("Users").doc(userID);
                        
                        return transaction.get(userDocRef).then(function(userDoc){
                            if(!userDoc.exists){
                                throw 400;
                            }
                            
                            // add teamID to user
                            var currentTIDs = userDoc.data().TIDs;
                            var currentTeamNames = userDoc.data().teamNames;
                            
                            currentTIDs.push(teamID);
                            currentTeamNames.push(displayName);
                            
                            transaction.update(userDocRef, {
                                TIDs: currentTIDs,
                                teamNames: currentTeamNames
                            });
                            
                            return teamID;
                            
                        });
                    }).then(function(teamID){
                        console.log("New team created with TID " + teamID + "; created by " + userID + ".");
                        myResponse.status = 200;
                        myResponse.json({"TID":teamID});
                        myResponse.end();
                    }).catch(function(error){
                        teamDoc.delete();
                        throw error;
                    });
                })
                .catch(function(error){
                    if(error == 400){
                        myErrorMessage = "400 Error: UID " + userID + " not found.";
                        utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                    }
                    else{
                        var myErrorMessage = "500 Error: Unable to add new team, error returned from database: " + error;
                        utils.returnError(myResponse, myErrorMessage, 500);
                    }
                });
            }
        });

         /*
         * GET request endpoint which attempts to fetch the user ids (UIDs) of all members belonging to the team 
         * with the id requested (TID)
         *
         * query requirements:  - TID : string
         * body requirements:   None.
         *
         * returned JSON:       - found : bool; true if team is present in the database
         *                      - UIDs : array of strings; empty array if team is not present in the database
         *
         * returns error if UID is not present in query, found is false and teams is an empty array on error
         */
         app.get("/getUIDs", function(request, myResponse) {    
            
            var teamID = request.query["TID"];
            
            if(typeof teamID !== "string" || teamID.length <= 0){
                var myErrorMessage = "400 Error: Expected TID in getUIDs as a non empty string";
                utils.returnError(myResponse, myErrorMessage, 400, {"found":false, "UIDs":[]});
            } 
            else{
                // query database and find a which key corresponds to the product
                var docRef = db.collection("Teams").doc(teamID);
                
                docRef.get().then(function(doc){
                    myResponse.status = 200;
                    
                    if(!doc.exists){
                        console.log("TID " + teamID + " is invalid, returned empty array");
                        
                        myResponse.json({"found":false, "UIDs":[]});
                        
                        myResponse.end();
                    } else{
                        console.log("TID " + teamID + " is valid, returned UIDs");
                        
                        myResponse.json({"found":true, "UIDs":doc.data().UIDs});
                        
                        myResponse.end();
                    }
                });
            }
            
        });

        /*
         * PUT request endpoint which adds a user to an existing team in the database
         *
         * query requirements:  None
         * body requirements:   - UID : string
         *                      - TID : string
         *
         * returned JSON:       - updated : bool; true if user was present in database and was updated
         *
         * returns error if: requirements are not correct type or are not present in request body, updated false on error
         *                   UID is not present in the user database
         *                   TID is not present in the user database   
         */
        app.put("/addToTeam", function(myRequest, myResponse){
            // Retrieve TID and UID from query
            var teamID = myRequest.body.TID;
            var userID = myRequest.body.UID;
            
            if(utils.checkIfUndefined([teamID, userID])){
                var myErrorMessage = "400 Error: Expected addToTeam PUT request to contain UID and TID as non empty strings";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else if(!utils.checkIfStrings([teamID, userID])){
                var myErrorMessage = "400 Error: Expected addToTeam PUT request to contain UID and TID as non empty strings";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var teamDocRef = db.collection("Teams").doc(teamID);
                var userDocRef = db.collection("Users").doc(userID);
                
                db.runTransaction(function(transaction){
                    return transaction.get(userDocRef).then(function(userDoc){                     
                        if(!userDoc.exists){
                            throw 400;
                        }
                        
                        return transaction.get(teamDocRef).then(function(teamDoc){                     
                            if(!teamDoc.exists){
                                throw 400;
                            }
                            
                            // both docs exist - see if user is already a part of the team
                            var currentUIDs = teamDoc.data().UIDs;
                            var currentTIDs = userDoc.data().TIDs;
                            var currentTeamNames = userDoc.data().teamNames;
                            
                            var indexU = currentUIDs.indexOf(userID);
                            var indexT = currentTIDs.indexOf(teamID);
                            
                            if(indexU > -1 || indexT > -1){
                                // already a member
                                return false;
                            }
                            
                            currentUIDs.push(userID);
                            
                            var displayName = teamDoc.data().displayName;
                            currentTIDs.push(teamID);
                            currentTeamNames.push(displayName);
                            
                            transaction.update(teamDocRef, {
                                UIDs: currentUIDs
                            });
                            
                            transaction.update(userDocRef, {
                                TIDs: currentTIDs,
                                teamNames: currentTeamNames
                            });
                            
                            return true;
                        });
                    });
                }).then(function(updated) {
                    // everything succeeded!
                    console.log("UID " + userID + " is a part of TID " + teamID);
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                }).catch(function(error){
                    if(error === 400){
                        var myErrorMessage = "400 Error: TID or UID does not exist";
                        utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                    }
                    else{
                        myErrorMessage = "500 Error: Unable to add user to team, error returned: " + error;
                        utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                    }
                });
            }
        });

        /*
         * PUT request endpoint which removes a user from a team
         *
         * query requirements:  None
         * body requirements:   - UID : string
         *                      - TID : string
         *
         * returned JSON:       - updated : bool; true if user was present in database and was updated
         *
         * returns error if: requirements are not correct type or are not present in request body, updated false on error
         *                   UID is not present in the user database
         *                   TID is not present in the user database   
         */
         app.put("/removeFromTeam", function(myRequest, myResponse){
            // Retrieve TID and UID from query
            var teamID = myRequest.body.TID;
            var userID = myRequest.body.UID;
            
            if(utils.checkIfUndefined([teamID, userID])){
                var myErrorMessage = "400 Error: Expected addToTeam PUT request to contain UID and TID as non empty strings";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else if(!utils.checkIfStrings([teamID, userID])){
                var myErrorMessage = "400 Error: Expected addToTeam PUT request to contain UID and TID as non empty strings";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var teamDocRef = db.collection("Teams").doc(teamID);
                var userDocRef = db.collection("Users").doc(userID);
                
                db.runTransaction(function(transaction){
                    return transaction.get(userDocRef).then(function(userDoc){
                        return transaction.get(teamDocRef).then(function(teamDoc){
                            var updated = false;
                            
                            if(userDoc.exists){
                                var currentTIDs = userDoc.data().TIDs;
                                var currentTeamNames = userDoc.data().teamNames;
                                
                                var index = currentTIDs.indexOf(teamID);
                                
                                if(index > -1){
                                    currentTIDs.splice(index, 1);
                                    currentTeamNames.splice(index, 1);
                                    
                                    transaction.update(userDocRef, {
                                        TIDs: currentTIDs,
                                        teamNames: currentTeamNames
                                    });
                                    
                                    updated = true;
                                }
                            }
                            if(teamDoc.exists){
                                var currentUIDs = teamDoc.data().UIDs;
                                
                                var index = currentUIDs.indexOf(userID);
                                
                                if(index > -1){
                                    currentUIDs.splice(index, 1);
                                        
                                    transaction.update(teamDocRef, {
                                        UIDs: currentUIDs
                                    });
                                    
                                    updated = true;
                                }
                            }
                            return updated;
                        });
                    });
                }).then(function(updated) {
                    // everything succeeded!
                    console.log("UID " + userID + " is no longer part of TID " + teamID);
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                }).catch(function(error){
                    myErrorMessage = "500 Error: Unable to add user to team, error returned: " + error;
                    utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                });
            }
        });

        /*
         * DELETE request endpoint which deletes the given team
         *
         * query requirements:  - TID : string
         * body requirements:   None
         *
         * returned JSON:       - updated : bool; true if team was present in database and was deleted
         *                                  if TID is not found in database, they are not updated.          
         *
         * returns error if TID is not present in query, updated false on error
         */
        app.delete("/deleteTeam", function(myRequest, myResponse) {
            var teamID = myRequest.query.TID;
            
            var myErrorMessage = "";
            
            if(typeof teamID !== "string" || teamID.length <= 0){
                myErrorMessage = "400 Error: Expected TID in deleteTeam as a string";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var docRef = db.collection("Teams").doc(teamID);
                
                return db.runTransaction(function(transaction) {
                    return transaction.get(docRef).then(function(doc) {
                        
                        if(!doc.exists) {
                            return [false, []];
                        }
                        
                        var allUserIDs = doc.data().UIDs;
                        
                        transaction.delete(docRef);
                        
                        return [true, allUserIDs];
                    });
                }).then(function(promise) {
                    var updated = promise[0];
                    var allUserIDs = promise[1];
                    
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                    
                    return allUserIDs
                }).catch(function(error) {
                    myErrorMessage = "500 Error: Unable to delete team, error returned: " + error;
                    utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                    return [];
                }).then(function(allUserIDs){
                    for(var i = 0; i < allUserIDs.length; i++){
                        // transaction failed - reverse changes made in the user's side
                        var removeUserFromTeamURL = "https://bitsplease-team-service.herokuapp.com/removeFromTeam";
                        axios.put(removeUserFromTeamURL, {
                            "UID" : allUserIDs[i],
                            "TID" : teamID
                        });
                    }
                });
            }
            
        });

        app.listen(app.get("port"), function() {
            console.log("Node app is running at localhost:" + app.get("port"));
        });
    }
};