var expect  = require("chai").expect;
var axios = require("axios");

var teamURL = "https://bitsplease-team-service.herokuapp.com/";
var userURL = "https://bitsplease-user-service.herokuapp.com/";

var newTeamURL = teamURL + "newTeam";
var getUIDsURL = teamURL + "getUIDs";
var addToTeamURL = teamURL + "addToTeam";
var removeFromTeamURL = teamURL + "removeFromTeam";
var deleteTeamURL = teamURL + "deleteTeam";

var newLoginURL = userURL + "newLogin/";
var deleteUserURL = userURL + "deleteUser";
var getCalTokenURL = userURL + "getCalToken";
var addUserToTeamURL = userURL + "addUserToTeam";
var getTeamsURL = userURL + "getTeams";
var removeUserFromTeamURL = userURL + "removeUserFromTeam";
        
// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

describe("Team Service", function() {
    this.timeout(6000);
    
    var existingUID = "001existingUser";
    var nonExistingUID = "001thisUserDoesNotExist";
    
    var existingTeam0 = ["01nonEmptyTeam","thisteamhasusers"];
    var myTeam = [undefined,"myTeamName"];
    var emptyTeam = ["01emptyTeam","01thisteamisempty"];
    var badUID = 1;
    
    var firstCallTID = "";
    var secondCallTID = "";
    
    var previousNumTeams;
    
    // ensures server is in the correct state for testing
    describe("Server is in the right state", function(){
        it("Succeeds in waking up the server", function(done){
            this.timeout(50000);
            axios.get(teamURL)
                .then(function (response) {
                    console.log(response.status);
                }).catch(function (error) {
                    expect(error.response.status).to.equal(404);
                }).then(function () {
                    done();
            });
        });
    });
    
    // ensures database is in the correct state for testing
    describe("Database is in the right state", function(){
        it("Returns that emptyTeam is empty", function(done){
            axios.get(getUIDsURL + "?TID=" + emptyTeam[0], {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(0);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
    });
    
    
    /*
     * Tests: newTeam POST request
     *          - Correct client input for existing users
     *          - Incorrect client input (non-existing users)
     *          - Incorrect client input (invalid variables)
     *          - Multiple calls at the same time with same UID
     */
    describe("newTeam", function() {
        previousNumTeams = 0;
        
        it("Returns {TID:string} with an existing user", function(done) {
            axios.post(newTeamURL, {
                "teamDisplayName": myTeam[1],
                "UID": existingUID
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("TID");
                    expect(typeof response.data.TID).to.equal("string");
                    
                    myTeam[0] = response.data.TID
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Added user to newly created team (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    previousNumTeams = teams.length;
                    
                    var myTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === myTeam[0] && team[1] === myTeam[1]){
                            myTeamFound = true;
                            break;
                        }
                    }
                    
                    expect(myTeamFound).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Added user to newly created team (on the Team side)", function(done) {
            axios.get(getUIDsURL + "?TID=" + myTeam[0], {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(existingUID);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if not given an existing user", function(done) {
            axios.post(newTeamURL, {
                "teamDisplayName": myTeam[1],
                "UID": nonExistingUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without UID", function(done) {
            axios.post(newTeamURL, {
                "teamDisplayName": "Team Display Name"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without teamDisplayName", function(done) {
            axios.post(newTeamURL, {
                "UID": existingUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without teamDisplayName and UID", function(done) {
            axios.post(newTeamURL, {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if UID is invalid (int)", function(done) {
            axios.post(newTeamURL, {
                "teamDisplayName": myTeam[1],
                "UID": 1
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if teamDisplayName is invalid (int)", function(done) {
            axios.post(newTeamURL, {
                "teamDisplayName": 1,
                "UID": existingUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Correctly did not add user to any additional teams", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams).to.equal(teams.length);
                    
                    var myTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === myTeam[0] && team[1] === myTeam[1]){
                            myTeamFound = true;
                            break;
                        }
                    }
                    
                    expect(myTeamFound).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds if called with same UID at the same time", function(done) {
            setTimeout(function(){
                axios.post(newTeamURL, {
                    "teamDisplayName": myTeam[1] + "1",
                    "UID": existingUID
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("TID");
                        expect(typeof response.data.TID).to.equal("string");
                        
                        firstCallTID = response.data.TID;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                });
            }, 500);
            
            setTimeout(function(){
                axios.post(newTeamURL, {
                    "teamDisplayName": myTeam[1] + "2",
                    "UID": existingUID
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("TID");
                        expect(typeof response.data.TID).to.equal("string");
                        
                        secondCallTID = response.data.TID;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                });
            }, 500);
            
            setTimeout(function(){
                done();
            }, 3000);
            
        });
            
        it("Added user to newly created teams (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams + 2).to.equal(teams.length);
                    previousNumTeams = teams.length;
                    
                    var myTeamFound = false;
                    var team1Found = false;
                    var team2Found = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === myTeam[0] && team[1] === myTeam[1]){
                            myTeamFound = true;
                        }
                        if(team[0] === firstCallTID && team[1] === myTeam[1] + "1"){
                            team1Found = true;
                        }
                        if(team[0] === secondCallTID && team[1] === myTeam[1] + "2"){
                            team2Found = true;
                        }
                    }
                    
                    expect(myTeamFound).to.equal(true);
                    expect(team1Found).to.equal(true);
                    expect(team2Found).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Added user to first team (on the Team side)", function(done) {
            axios.get(getUIDsURL + "?TID=" + firstCallTID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(existingUID);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Added user to second team (on the Team side)", function(done) {
            axios.get(getUIDsURL + "?TID=" + secondCallTID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(existingUID);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
    });
    
    /*
     * Tests: getUIDs GET request
     *          - Correct client input for existing and non-existing teams
     *          - Incorrect client input (no TID)
     */
    describe("getUIDs", function() {
        var newTeamURL = teamURL + "newTeam";
        var getUIDsURL = teamURL + "getUIDs";
        var getTeamsURL = userURL + "getTeams";
        
        var previousNumTeams = 0;
        
        it("Returns correctly with an existing team", function(done) {
            axios.get(getUIDsURL + "?TID=" + existingTeam0[0])
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("UIDs");
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(existingUID);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns correctly with an non-existing team", function(done) {
            axios.get(getUIDsURL + "?TID=" + Math.random().toString())
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("UIDs");
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(false);
                    
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    expect(response.data.UIDs.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without TID", function(done) {
            axios.get(getUIDsURL)
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("found");
                    expect(error.response.data.found).to.have.equal(false);
                    
                    expect(error.response.data.UIDs.constructor).to.equal(Array);
                    expect(error.response.data.UIDs.length).to.equal(0);
                }).then(function () {
                    done();
            });
        });
    });

    /*
     * Tests: addToTeam PUT request
     *          - Correct client input for existing team and user not in the team
     *          - Correct client input for existing team and user already in the team
     *          - Incorrect client input (invalid variables)
     *          - Multiple calls at the same time with same UID
     *          - Multiple calls at the same time with same TID
     *          - Multiple calls at the same time with same UID and TID
     */
    describe("addToTeam", function() {
        it("Returns correctly with an existing team and user not in team", function(done) {
            axios.put(addToTeamURL, {
                    "TID": emptyTeam[0],
                    "UID": existingUID
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Added user to team (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    previousNumTeams = teams.length;
                    
                    var emptyTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === emptyTeam[0] && team[1] === emptyTeam[1]){
                            emptyTeamFound = true;
                            break;
                        }
                    }
                    
                    expect(emptyTeamFound).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Added user to team (on the Team side)", function(done) {
            axios.get(getUIDsURL + "?TID=" + emptyTeam[0], {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(existingUID);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns correctly with an existing team and user already in team", function(done) {
            axios.put(addToTeamURL, {
                    "TID": emptyTeam[0],
                    "UID": existingUID
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Didn't user to team again (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams).to.equal(teams.length);
                    
                    var emptyTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === emptyTeam[0] && team[1] === emptyTeam[1]){
                            emptyTeamFound = true;
                            break;
                        }
                    }
                    
                    expect(emptyTeamFound).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Didn't add user to team again (on the Team side)", function(done) {
            axios.get(getUIDsURL + "?TID=" + emptyTeam[0], {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(existingUID);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without TID", function(done) {
            axios.put(addToTeamURL, {
                "UID": existingUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without UID", function(done) {
            axios.put(addToTeamURL, {
                "TID": emptyTeam[0]
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (bool)", function(done) {
            axios.put(addToTeamURL, {
                "TID": emptyTeam[0],
                "UID": true
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if TID does not exist", function(done) {
            axios.put(addToTeamURL, {
                "TID": "thisIsNotARealTeam",
                "UID": existingUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if UID does not exist", function(done) {
            axios.put(addToTeamURL, {
                "TID": emptyTeam[0],
                "UID": "thisIsNotARealUser"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
    });
    
    /*
     * Tests: removeFromTeam PUT request
     *          - Correct client input for existing team and user not in the team
     *          - Correct client input for existing team and user already in the team
     *          - Incorrect client input (invalid variables)
     *          - Multiple calls at the same time with same UID
     *          - Multiple calls at the same time with same TID
     *          - Multiple calls at the same time with same UID and TID
     */
    describe("removeFromTeam", function() {
        it("Successfully allows user to leave team", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": existingUID,
                "TID": myTeam[0]
            }).then(function (response) {
                expect(response.status).to.equal(200);
                expect(response.data).to.have.property("updated");
                expect(response.data.updated).to.have.equal(true);
            }).catch(function (error) {
                console.log(error); // should not be reached
                expect(1).to.equal(-1);
            }).then(function () {
                    done();
            });
        });
        
        it("Removed team from the user (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams - 1).to.equal(teams.length);
                    previousNumTeams = teams.length;
                    
                    var myTeamFound = false;
                    var team1Found = false;
                    var team2Found = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === myTeam[0] && team[1] === myTeam[1]){
                            myTeamFound = true;
                        }
                        if(team[0] === firstCallTID && team[1] === myTeam[1] + "1"){
                            team1Found = true;
                        }
                        if(team[0] === secondCallTID && team[1] === myTeam[1] + "2"){
                            team2Found = true;
                        }
                    }
                    
                    expect(myTeamFound).to.equal(false);
                    expect(team1Found).to.equal(true);
                    expect(team2Found).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
            
        });
        
        it("Successfully returns when user is not a member of the team", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": existingUID,
                "TID": myTeam[0]
            }).then(function (response) {
                expect(response.status).to.equal(200);
                expect(response.data).to.have.property("updated");
                expect(response.data.updated).to.have.equal(false);
            }).catch(function (error) {
                console.log(error); // should not be reached
                expect(1).to.equal(-1);
            }).then(function () {
                    done();
            });
        });
        
        it("Didn't remove the user from any teams (on the user side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams).to.equal(teams.length);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
            
        });
        
        it("Fails with status code 400 without TID", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": existingUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without UID", function(done) {
            axios.put(removeFromTeamURL, {
                "TID": existingTeam0[0]
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without UID and TID", function(done) {
            axios.put(removeFromTeamURL, {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid TID (empty string)", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": existingUID,
                "TID": ""
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (int)", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": 1,
                "TID": existingTeam0[0]
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                    
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        // TODO
        
        it("Successfully allows user to leave team", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": existingUID,
                "TID": emptyTeam[0]
            }).then(function (response) {
                expect(response.status).to.equal(200);
                expect(response.data).to.have.property("updated");
                expect(response.data.updated).to.have.equal(true);
            }).catch(function (error) {
                console.log(error); // should not be reached
                expect(1).to.equal(-1);
            }).then(function () {
                    done();
            });
        });
        
        it("Removed team from the user (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams - 1).to.equal(teams.length);
                    previousNumTeams = teams.length;
                    
                    var emptyTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === emptyTeam[0] && team[1] === emptyTeam[1]){
                            myTeamFound = true;
                        }
                    }
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
            
        });
    });
    
    /*
     * Tests: deleteTeam DELETE request
     *          - Correct client input for existing team
     *          - Correct client input for non-existing team
     *          - Incorrect client input (invalid TID)
     *          - Multiple calls at the same time with same TID
     */
    describe("deleteTeam", function() {
        it("Succeeds in deleting team with users", function(done){
            axios.delete(deleteTeamURL + "?TID=" + firstCallTID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
            
        it("Removed team from the user (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    
                    expect(previousNumTeams - 1).to.equal(teams.length);
                    previousNumTeams = teams.length;
                    
                    var myTeamFound = false;
                    var team1Found = false;
                    var team2Found = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === myTeam[0] && team[1] === myTeam[1]){
                            myTeamFound = true;
                        }
                        if(team[0] === firstCallTID && team[1] === myTeam[1] + "1"){
                            team1Found = true;
                        }
                        if(team[0] === secondCallTID && team[1] === myTeam[1] + "2"){
                            team2Found = true;
                        }
                    }
                    
                    expect(myTeamFound).to.equal(false);
                    expect(team1Found).to.equal(false);
                    expect(team2Found).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
            
        });
        
        it("Succeeds in deleting team without users", function(done){
            axios.delete(deleteTeamURL + "?TID=" + myTeam[0])
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
            
        it("Existing user's teams remain the same (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams).to.equal(teams.length);
                    
                    var myTeamFound = false;
                    var team1Found = false;
                    var team2Found = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === myTeam[0] && team[1] === myTeam[1]){
                            myTeamFound = true;
                        }
                        if(team[0] === firstCallTID && team[1] === myTeam[1] + "1"){
                            team1Found = true;
                        }
                        if(team[0] === secondCallTID && team[1] === myTeam[1] + "2"){
                            team2Found = true;
                        }
                    }
                    
                    expect(myTeamFound).to.equal(false);
                    expect(team1Found).to.equal(false);
                    expect(team2Found).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
            
        it("Succeeds in deleting non-exisiting team", function(done){
            axios.delete(deleteTeamURL + "?TID=" + myTeam[0] + "222")
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without TID", function(done) {
            axios.delete(deleteTeamURL)
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid TID (empty string)", function(done) {
            axios.delete(deleteTeamURL + "?TID=" + "")
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
            
        var firstCallUpdated = false;
        var secondCallUpdated = false;
        
        it("Succeeds if called with same TID at the same time", function(done) {
            setTimeout(function(){
                axios.delete(deleteTeamURL + "?TID=" + secondCallTID)
                    .then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        firstCallUpdated = response.data.updated;
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                });
            }, 500);
            
            setTimeout(function(){
                axios.delete(deleteTeamURL + "?TID=" + secondCallTID)
                    .then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        secondCallUpdated = response.data.updated;
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                });
            }, 500);
            
            setTimeout(function(){
                expect(firstCallUpdated || secondCallUpdated).to.equal(true);
                expect(firstCallUpdated && secondCallUpdated).to.equal(false);
                done();
            }, 3000);
            
        });
        
        it("Removed team from the user (on the User side)", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(previousNumTeams - 1).to.equal(teams.length);
                    previousNumTeams = teams.length;
                    
                    var myTeamFound = false;
                    var team1Found = false;
                    var team2Found = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === myTeam[0] && team[1] === myTeam[1]){
                            myTeamFound = true;
                        }
                        if(team[0] === firstCallTID && team[1] === myTeam[1] + "1"){
                            team1Found = true;
                        }
                        if(team[0] === secondCallTID && team[1] === myTeam[1] + "2"){
                            team2Found = true;
                        }
                    }
                    
                    expect(myTeamFound).to.equal(false);
                    expect(team1Found).to.equal(false);
                    expect(team2Found).to.equal(false);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
    });
    
    describe("Expected usecase", function() {
        var userID1 = "001existingUser";
        var initialTeamCount = 0;

        var userID2 = "001existingUser2";
        
        var teamID = undefined;
        var teamName = Math.random().toString();
        
        it("Returns teams for user1", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID1, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    initialTeamCount = teams.length;
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns no Teams for user2", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID2, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    expect(response.data.teams.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully allows user1 to create new team", function(done) {
            axios.post(newTeamURL, {
                "teamDisplayName": teamName,
                "UID": userID1
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("TID");
                    expect(typeof response.data.TID).to.equal("string");
                    
                    teamID = response.data.TID;
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });

        it("Returns newly created team has one user", function(done) {
            axios.get(getUIDsURL + "?TID=" + teamID
            , {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(userID1);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns user1 is a member of the newly created team", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID1, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(teams.length).to.equal(initialTeamCount + 1);
                    
                    var newTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === teamID && team[1] === teamName){
                            newTeamFound = true;
                        }
                    }
                    expect(newTeamFound).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully allows user2 to join newly created team", function(done) {
            axios.put(addToTeamURL, {
                "UID": userID2,
                "TID": teamID
            }).then(function (response) {
                expect(response.status).to.equal(200);
                expect(response.data).to.have.property("updated");
                expect(response.data.updated).to.have.equal(true);
            }).catch(function (error) {
                console.log(error); // should not be reached
                expect(1).to.equal(-1);
            }).then(function () {
                    done();
            });
        });

        it("Returns newly created team has two users", function(done) {
            axios.get(getUIDsURL + "?TID=" + teamID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(2);
                    
                    expect(response.data.UIDs.indexOf(userID1)).to.be.within(0,1);
                    expect(response.data.UIDs.indexOf(userID2)).to.be.within(0,1);
                    
                    expect(response.data.UIDs.indexOf(userID1)).to.not.equal(response.data.UIDs.indexOf(userID2));
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns user2 is a member of the newly created team", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID2, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    expect(response.data.teams.length).to.equal(1);
                    
                    var team = response.data.teams[0];
                    
                    expect(team.length).to.equal(2);
                        
                    expect(typeof team[0]).to.equal("string");
                    expect(typeof team[1]).to.equal("string");
                    
                    expect(team[0]).to.equal(teamID);
                    expect(team[1]).to.equal(teamName);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully allows user1 to leave newly created team", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": userID1,
                "TID": teamID
            }).then(function (response) {
                expect(response.status).to.equal(200);
                expect(response.data).to.have.property("updated");
                expect(response.data.updated).to.have.equal(true);
            }).catch(function (error) {
                console.log(error); // should not be reached
                expect(1).to.equal(-1);
            }).then(function () {
                    done();
            });
        });
        
        it("Returns newly created team has one user", function(done) {
            axios.get(getUIDsURL + "?TID=" + teamID
            , {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(1);
                            
                    expect(response.data.UIDs[0]).to.equal(userID2);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns user1 is not a member of the newly created team", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID1, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(teams.length).to.equal(initialTeamCount);
                    
                    var newTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === teamID && team[1] === teamName){
                            newTeamFound = true;
                        }
                    }
                    expect(newTeamFound).to.equal(false);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns user2 is a member of the newly created team", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID2, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    expect(response.data.teams.length).to.equal(1);
                    
                    var team = response.data.teams[0];
                    
                    expect(team.length).to.equal(2);
                        
                    expect(typeof team[0]).to.equal("string");
                    expect(typeof team[1]).to.equal("string");
                    
                    expect(team[0]).to.equal(teamID);
                    expect(team[1]).to.equal(teamName);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully allows user2 to leave newly created team", function(done) {
            axios.put(removeFromTeamURL, {
                "UID": userID2,
                "TID": teamID
            }).then(function (response) {
                expect(response.status).to.equal(200);
                expect(response.data).to.have.property("updated");
                expect(response.data.updated).to.have.equal(true);
            }).catch(function (error) {
                console.log(error); // should not be reached
                expect(1).to.equal(-1);
            }).then(function () {
                    done();
            });
        });
        
        it("Returns newly created team has no users", function(done) {
            axios.get(getUIDsURL + "?TID=" + teamID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("UIDs");
                    expect(response.data.UIDs.constructor).to.equal(Array);
                    
                    expect(response.data.UIDs.length).to.equal(0);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns user1 is not a member of the newly created team", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID1, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    var teams = response.data.teams;
                    expect(teams.length).to.equal(initialTeamCount);
                    
                    var newTeamFound = false;
                    
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === teamID && team[1] === teamName){
                            newTeamFound = true;
                        }
                    }
                    expect(newTeamFound).to.equal(false);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns user2 is not a member of the newly created team", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID2, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    expect(response.data.teams.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully deletes team", function(done) {
            axios.delete(deleteTeamURL + "?TID=" + teamID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
    });
});