var expect  = require("chai").expect;
var axios = require("axios");

var teamURL = "https://bitsplease-team-service.herokuapp.com/";
var userURL = "https://bitsplease-user-service.herokuapp.com/";
var calendarURL = "https://bitsplease-calendar-service.herokuapp.com/";

var newTeamURL = teamURL + "newTeam";
var getUIDsURL = teamURL + "getUIDs";
var addToTeamURL = teamURL + "addToTeam";
var removeFromTeamURL = teamURL + "removeFromTeam";
var deleteTeamURL = teamURL + "deleteTeam";

var newLoginURL = userURL + "newLogin/";
var deleteUserURL = userURL + "deleteUser";
var getCalTokenURL = userURL + "getCalToken";
var addUserToTeamURL = userURL + "addUserToTeam";
var getTeamsURL = userURL + "getTeams";
var removeUserFromTeamURL = userURL + "removeUserFromTeam";

var getCalEventsURL = calendarURL + "getCalEvents";
var getAuthURL = calendarURL + "getAuthURL";
var exchangeCalCodeURL = calendarURL + "exchangeCalCode";
        
// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

describe("Calendar Service", function() {
    this.timeout(6000);
    var noTokenUID = "004noCalToken";
    var hasTokenUID = "004hasCalToken";
    var invalidTokenUID = "004hasBadToken";
    var validCalCode0 = "4/sgAKufYLvsEmb0doQtf25eTfQ5ZnyLy4k4lAQzYIgYjSeONxGEFk7ew";
    var validCalCode1 = "4/sgDtHCO-PNxswMe7UH-JAjtaY35xX5qe6OwDC6-91NjLgODW8foVusY";
    
    // ensures server is in the correct state for testing
    describe("Server is in the right state", function(){
        it("Succeeds in waking up the server", function(done){
            this.timeout(50000);
            axios.get(calendarURL)
                .then(function (response) {
                    console.log(response.status);
                }).catch(function (error) {
                    expect(error.response.status).to.equal(404);
                }).then(function () {
                    done();
            });
        });
    });
    
    
    describe("getCalEvents", function(){
        it("Succeeds with user with calToken", function(done){
            axios.get(getCalEventsURL + "?UID=" + hasTokenUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("tokenFound");
                    expect(response.data.tokenFound).to.have.equal(true);
                    
                    expect(response.data).to.have.property("events");
                    expect(response.data.events.constructor).to.equal(Array);
                    
                    var events = response.data.events;
                    
                    for(var i = 0; i < events.length; i++){
                        var oneEvent = events[i];
                        
                        expect(oneEvent.length).to.equal(3);
                        
                        var name = oneEvent[0];
                        
                        if(name === null){
                            name = "";
                        }
                        
                        expect(typeof name).to.equal("string");
                        expect(typeof oneEvent[1]).to.equal("string");
                        expect(typeof oneEvent[2]).to.equal("string");
                    }
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds with user with no calToken", function(done){
            axios.get(getCalEventsURL + "?UID=" + noTokenUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("tokenFound");
                    expect(response.data.tokenFound).to.have.equal(false);
                    
                    expect(response.data).to.have.property("events");
                    expect(response.data.events.constructor).to.equal(Array);
                    expect(response.data.events.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without UID", function(done) {
            axios.get(getCalEventsURL)
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("tokenFound");
                    expect(error.response.data.tokenFound).to.have.equal(false);
                    
                    expect(error.response.data).to.have.property("events");
                    expect(error.response.data.events.constructor).to.equal(Array);
                    expect(error.response.data.events.length).to.equal(0);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (empty string)", function(done) {
            axios.get(getCalEventsURL + "?UID=" + "")
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("tokenFound");
                    expect(error.response.data.tokenFound).to.have.equal(false);
                    
                    expect(error.response.data).to.have.property("events");
                    expect(error.response.data.events.constructor).to.equal(Array);
                    expect(error.response.data.events.length).to.equal(0);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with valid UID with invalid calToken", function(done) {
            axios.get(getCalEventsURL + "?UID=" + invalidTokenUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("tokenFound");
                    expect(response.data.tokenFound).to.have.equal(false);
                    
                    expect(response.data).to.have.property("events");
                    expect(response.data.events.constructor).to.equal(Array);
                    expect(response.data.events.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with valid UID not found in database", function(done) {
            axios.get(getCalEventsURL + "?UID=" + "notanexisitinguser")
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("tokenFound");
                    expect(error.response.data.tokenFound).to.have.equal(false);
                    
                    expect(error.response.data).to.have.property("events");
                    expect(error.response.data.events.constructor).to.equal(Array);
                    expect(error.response.data.events.length).to.equal(0);
                }).then(function () {
                    done();
            });
        });
    });
    
    
    describe("getAuthURL", function(){
        it("Succeeds when called", function(done){
            axios.get(getAuthURL, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("url");
                    expect(typeof response.data.url).to.equal("string");
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
    });
    
    
    describe("exchangeCalCode", function(){
        it("Fails with status code 400 invalid UID (does not exist)", function(done) {
            axios.put(exchangeCalCodeURL, {
                "calCode": validCalCode0,
                "UID": hasTokenUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Exchanges a valid CalCode", function(done) {
            axios.put(exchangeCalCodeURL, {
                "UID": hasTokenUID,
                "calCode": validCalCode1
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function(){
                    done();
            });
        });
        
        it("Fails with status code 400 without UID", function(done) {
            axios.put(exchangeCalCodeURL, {
                "calCode": validCalCode1
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without calCode", function(done) {
            axios.put(exchangeCalCodeURL, {
                "UID": hasTokenUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 invalid calCode (reused)", function(done) {
            axios.put(exchangeCalCodeURL, {
                "calCode": validCalCode1,
                "UID": hasTokenUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 invalid UID (int)", function(done) {
            axios.put(exchangeCalCodeURL, {
                "calCode": validCalCode1,
                "UID": 1
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.have.equal(false);
                }).then(function () {
                    done();
            });
        });
    });
});