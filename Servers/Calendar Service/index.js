var im = require('istanbul-middleware');
var isCoverageEnabled = (true);

//before your code is require()-ed, hook the loader for coverage
if (isCoverageEnabled) {
    console.log('Hook loader for coverage - ensure this is not production!');
    
    console.log(__dirname);
    im.hookLoader(__dirname);
    // cover all files except under node_modules
    // see API for other options
}

const express = require("express");
const path = require("path");
const app = express();
const firebase = require("firebase");
const axios = require("axios");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const admin = require("firebase-admin");
const fs = require('fs');
const {google} = require('googleapis');
const globMeta = require(__dirname);
const utils = require("utils");

require('./calendarserver').start();