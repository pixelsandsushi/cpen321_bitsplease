const im = require('istanbul-middleware');
var isCoverageEnabled = (true);

const express = require("express");
const path = require("path");
const app = express();
const firebase = require("firebase");
const axios = require("axios");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const admin = require("firebase-admin");
const globMeta = require(__dirname);
const fs = require('fs');
const {google} = require('googleapis');
const rp = require("request-promise");

const utils = require("utils");

var db = {};
// Load service account secrets from a local file.
fs.readFile('/app/calendarserver/firestoreadmin.json', (err, content) => {
    if (err) return console.log('Error loading firestore admin file:', err);

    // Initialize Firebase
    admin.initializeApp({
        credential: admin.credential.cert(JSON.parse(content)),
        apiKey: "AIzaSyDYD4WI1hRgBOxM5IOs3YEx0T0NtlXMuqk",
        authDomain: "cpen321-users.firebaseapp.com",
        databaseURL: "https://cpen321-users.firebaseio.com",
        projectId: "cpen321-users",
        storageBucket: "cpen321-users.appspot.com",
        messagingSenderId: "285954961097"
    });
    
    // Get a reference to the firestore service
    db = admin.firestore();
});

// Load client secrets from a local file.
var clientJSON = {};
var client_id = "";
var client_secret = "";
var redirect_uris = "";
fs.readFile('/app/calendarserver/credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Calendar API.
  
    clientJSON = JSON.parse(content);

    client_secret = clientJSON.installed.client_secret;
    client_id = clientJSON.installed.client_id;
    redirect_uris = clientJSON.installed.redirect_uris;
});

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

/*==============================================
 * Assorted helper functions
 *==============================================*/
function getEventsProm(auth) {
    const calendar = google.calendar({version: 'v3', auth});
    
    return new Promise(function (resolve, reject) {
        calendar.events.list({
            calendarId: 'primary',
            timeMin: (new Date()).toISOString(),
            maxResults: 10,
            singleEvents: true,
            orderBy: 'startTime'
        }, function (err, res) {
            console.log("here");
            
            if (err){
                console.log(err);
                res = -1;
            };
            
            console.log(res);
            
            resolve(res);
        });
    });
}

function getCalendar(calToken, myResponse){
    async function getCalendarAsync(){ 
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        oAuth2Client.setCredentials(JSON.parse(calToken));
        
        var resource = {
            timeMin: (new Date()).toISOString(),
            timeMax: (new Date("December 17, 2018 03:24:00")).toISOString(),
            items:[{id: 'primary'}]
        }
                                
        var res = await getEventsProm(oAuth2Client);
        
        if(res === -1){
            myResponse.status(200);
            myResponse.json({tokenFound:false, "events":[]});
            myResponse.end();
        }
        else{
            var events = res.data.items;
            
            var eventsToReturn = [];
            Object.keys(events).forEach(function(key) {
                var calEvent = events[key];
                var timeStr = new Date(calEvent.start.dateTime).toUTCString();
                var eventStart = timeStr.substring(0, timeStr.length - 7);
                
                timeStr = new Date(calEvent.end.dateTime).toUTCString();
                var eventEnd = timeStr.substring(0, timeStr.length - 7);
                
                eventsToReturn.push([calEvent.summary, eventStart, eventEnd]);
            });
            
            console.log(eventsToReturn);
            myResponse.status(200);
            myResponse.json({tokenFound:true, "events":eventsToReturn});
            myResponse.end();
        }
    }
    
    getCalendarAsync().catch(err => console.log)
}

module.exports = {
    start: function(){
        app.set("port", (process.env.PORT || 5000));

        app.use(express.json());
        app.use(express.urlencoded());
        
        // for testing only - remove if deployed
        app.use('/coverage', im.createHandler());

        app.use(function(req,res,next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type,Accept");
            next();
        });
        
        // allows access from local host
        app.use("/", express.static(path.join(__dirname, "..")));

        app.listen(app.get("port"), function() {
          console.log("Node app is running at localhost:" + app.get("port"));
        });

        /*==============================================
         * GET, POST, PUT requests
         *==============================================*/
         
        /*
         * GET request endpoint which returns the calendar events of a given user
         *
         * query requirements:  - UID : string
         * body requirements:   None
         *
         * returned JSON:       - tokenFound : bool
         *                      - events : array
         *
         * returns error if UID not present in database or not present in query, tokenFound is false and events is an empty array on error
         */

        app.get("/getCalEvents", function(myRequest, myResponse) {
            var userID = myRequest.query["UID"];
            var myErrorMessage = "";
            
            console.log("getCalEvents");
            
            if(typeof userID !== "string" || userID.length <= 0){
                myErrorMessage = "400 Error: Expected string UID in query of getCalToken GET request.";
                utils.returnError(myResponse, myErrorMessage, 400, {"tokenFound":false, "events":[]});
            }
            else{
                var docRef = db.collection("Users").doc(userID);
                
                docRef.get().then(function(doc){
                    console.log("got db doc");
                    
                    if(!doc.exists){
                        myErrorMessage = "400 Error: Expected UID to be present in the database";
                        utils.returnError(myResponse, myErrorMessage, 400, {"tokenFound":false, "events":[]});
                    }
                    else{
                        var calToken = doc.data().calToken;
                        console.log("got calToken");
                        
                        if(typeof calToken === "undefined"){
                            myResponse.status(200);
                            myResponse.json({"tokenFound":false, "events":[]});
                            myResponse.end();
                        }
                        else{
                            // try to fetch events from Google API
                            console.log("tryin to get events");
                            
                            getCalendar(calToken, myResponse);
                        }
                    }
                }).catch(function(error){
                    myErrorMessage = "500 Error: Unable to retrive user from database; error returned: " + error;
                    return utils.returnError(myResponse, myErrorMessage, 500);
                });
            }
        });
        
        /*
         * GET request endpoint which returns a url to visit 
         *
         * query requirements:  None
         * body requirements:   None
         *
         * returned JSON:       - url: string
         */
        app.get("/getAuthURL", function(myRequest, myResponse) {
            const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
            
            const authUrl = oAuth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: ['https://www.googleapis.com/auth/calendar']
            });
            
            console.log(authUrl);
            
            myResponse.status(200);
            myResponse.json({"url": authUrl});
            myResponse.end();
        });
        
        /*
         * PUT request endpoint which exchanges the given code for an access token
         *
         * query requirements:  None
         * body requirements:   - UID : string
         *                      - calCode : string
         *
         * returned JSON:       - updated : bool; true if user was present in database and was updated
         *                                  if the given calCode was invalid, returns updated as false         
         *
         * returns error if UID, calCode is not present in body as strings, updated false on error
         */
         
        app.put("/exchangeCalCode", function(myRequest, myResponse) {
            var userID = myRequest.body.UID;
            var calCode = myRequest.body.calCode;
            
            var myErrorMessage = "";
            
            if(utils.checkIfUndefined([userID, calCode])){
                myErrorMessage = "400 Error: Expected UID, and calCode in exchangeCalCode";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else if(!utils.checkIfStrings([userID, calCode])){
                myErrorMessage = "400 Error: Expected UID, TID, and teamName to be strings in exchangeCalCode";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
                oAuth2Client.getToken(calCode, (err, token) => {
                    if (err){
                        myErrorMessage = "400 Error: Invalid calCode, error returned: " + err;
                        return utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                    }
                    
                    var docRef = db.collection("Users").doc(userID);
                    db.runTransaction(function(transaction) {
                        return transaction.get(docRef).then(function(doc) {
                            if(!doc.exists) {
                                throw 400;
                            }
                            
                            transaction.update(docRef, {
                                "calToken": JSON.stringify(token)
                            });
                            
                            return true;
                        });
                    }).then(function(updated) {
                        console.log("UID " + userID + " now has a calendar token.");
                        myResponse.status = 200;
                        myResponse.json({updated});
                        myResponse.end();
                        
                    }).catch(function(error){
                        if(error === 400){
                            myErrorMessage = "400 Error: User not found in database.";
                            return utils.returnError(myResponse, myErrorMessage, 400, {"updated":false}); 
                        }
                        else{
                            myErrorMessage = "500 Error: Unable to update calToken: " + error;
                            utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                        }
                    });
                });
            }
        });
    }
}
