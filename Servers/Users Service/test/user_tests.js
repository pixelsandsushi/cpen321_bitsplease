var expect  = require("chai").expect;
var axios = require("axios");

var userURL = "https://bitsplease-user-service.herokuapp.com/";

var newLoginURL = userURL + "newLogin/";
var deleteUserURL = userURL + "deleteUser";
var getCalTokenURL = userURL + "getCalToken";
var addUserToTeamURL = userURL + "addUserToTeam";
var getTeamsURL = userURL + "getTeams";
var removeUserFromTeamURL = userURL + "removeUserFromTeam";

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

describe("User Service", function() {
    this.timeout(5000);
    
    var randomUID = Math.random().toString();
    var randomToken = "This is a pseudo token";
    
    var existingUID = "KiAA1qvDifHEwzUg3UmJ";
    var existingToken = "{\"access_token\":\"ya29.GltpBuD0h6Hmn39Ytc1_fFUd5t6sy6fziVQDN__NP7KzriRCms5DrAZNXCj2go6GqfsT3a7NYFmqrQkG4Qg2Wuo2M1ievDNf9BPktpFmFH8fYwRliNzn81N8OwNH\",\"refresh_token\":\"1/zlQcLECFCNCsdDBLgH-GdmXx8HRlZjcCWmvEseBIucU\",\"scope\":\"https://www.googleapis.com/auth/calendar.readonly\",\"token_type\":\"Bearer\",\"expiry_date\":1544050514394}";
    var existingTeam0 = ["TID0","TeamName0"];
    var existingTeam1 = ["TID1","TeamName1"];
    var existingTeam2 = ["TID2","TeamName2"];
    var newTeam = ["newTID","newTeamName"];
    
    var badUID = 1;
    
    var randomUID2 = Math.random().toString();
    
    // ensures server is in the correct state for testing
    describe("Server is in the right state", function(){
        it("Succeeds in waking up the server", function(done){
            this.timeout(50000);
            axios.get(userURL)
                .then(function (response) {
                    console.log(response.status);
                }).catch(function (error) {
                    expect(error.response.status).to.equal(404);
                }).then(function () {
                    done();
            });
        });
    });
    
    // ensures database is in the correct state for testing
    describe("Database is in the right state", function(){
        it("Succeeds in cleaning up user database", function(done){
            axios.delete(deleteUserURL + "?UID=" + existingUID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        
        it("Has existing user", function(done){
            axios.post(newLoginURL, {
                "displayName": "Existing UID Display Name",
                "UID": existingUID,
                "calToken": existingToken
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Has user in team 0", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": existingUID,
                "TID": existingTeam0[0],
                "teamName": existingTeam0[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function(){
                    done();
            });
        });
        
        it("Has user in team 1", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": existingUID,
                "TID": existingTeam1[0],
                "teamName": existingTeam1[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function(){
                    done();
            });
        });
        
        it("Has user in team 2", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": existingUID,
                "TID": existingTeam2[0],
                "teamName": existingTeam2[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function(){
                    done();
            });
        });
    });
    
    /*
     * Tests: newLogin POST request
     *          - Correct client input for new and old users
     *          - Incorrect client input
     *          - Multiple calls at the same time with same UID
     */
    describe("newLogin", function() {
        it("Returns {new:true} if new login", function(done) {
            axios.post(newLoginURL, {
                "displayName": "Random UID Display Name",
                "UID": randomUID,
                "calToken": randomToken
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("new");
                    expect(response.data.new).to.have.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns {new:false} if not new login", function(done) {
            axios.post(newLoginURL, {
                "displayName": "Existing UID Display Name",
                "UID": existingUID,
                "calToken": existingToken
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("new");
                    expect(response.data.new).to.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without UID", function(done) {
            axios.post(newLoginURL, {
                "displayName": "Display Name",
                "calToken": "Calendar Token"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with empty UID", function(done) {
            axios.post(newLoginURL, {
                "UID": "",
                "displayName": "Display Name",
                "calToken": "Calendar Token"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without displayName", function(done) {
            axios.post(newLoginURL, {
                "UID": "User ID",
                "calToken": "Calendar Token"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without displayName and UID", function(done) {
            axios.post(newLoginURL, {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (int)", function(done) {
            axios.post(newLoginURL, {
                "displayName": "Bad UID Display Name",
                "UID": badUID
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds if called with same UID at the same time", function(done) {
            var firstCallUpdated = false;
            var secondCallUpdated = false;
            
            setTimeout(function(){
                axios.post(newLoginURL, {
                    "displayName": "Random UID Display Name",
                    "UID": randomUID2,
                    "calToken": randomToken
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("new");
                        firstCallUpdated = response.data.new;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                        
                });
            }, 500);
            
            setTimeout(function(){
                axios.post(newLoginURL, {
                    "displayName": "Random UID Display Name",
                    "UID": randomUID2,
                    "calToken": randomToken
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("new");
                        secondCallUpdated = response.data.new;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                });
            }, 500);
            
            setTimeout(function(){
                // one of the two function calls should have been a new user
                expect(firstCallUpdated || secondCallUpdated).to.equal(true);
                
                // confirms user 1 is now a user, assumes getTeams is working correctly
                axios.get(getTeamsURL + "?UID=" + randomUID2)
                    .then(function (response) {
                        expect(response).to.have.property("status");
                        expect(response.status).to.equal(200);
                        
                        expect(response.data).to.have.property("found");
                        expect(response.data.found).to.have.equal(true);
                        
                        expect(response.data).to.have.property("teams");
                        expect(response.data.teams.constructor).to.equal(Array);
                        
                        expect(response.data.teams.length).to.equal(0);
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                });
                done();
            }, 3000);
        });
    });
    

    /*
     * Tests: getCalToken GET request
     *          - Correct client input for existing users
     *          - Incorrect client input
     */
    describe("getCalToken", function() {
        it("Returns {calToken:string} if UID is valid and in database", function(done) {
            axios.get(getCalTokenURL + "?UID=" + randomUID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("calToken");
                    expect(response.data.calToken).to.have.equal(randomToken);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });

        it("Fails with status code 400 if UID is valid and not in database", function(done) {
            axios.get(getCalTokenURL + "?UID=" + randomUID + "22")
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    expect(error.response.data).to.have.property("calToken");
                    expect(error.response.data.calToken).to.equal("");
                }).then(function () {
                    done();
            });
        });

        it("Fails with status code 400 without UID", function(done) {
            axios.get(getCalTokenURL)
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    expect(error.response.data).to.have.property("calToken");
                    expect(error.response.data.calToken).to.equal("");
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if UID is invalid (int)", function(done) {
            axios.get(getCalTokenURL + "?UID=" + badUID)
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    expect(error.response.data).to.have.property("calToken");
                    expect(error.response.data.calToken).to.equal("");
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if UID is invalid (empty string)", function(done) {
            axios.get(getCalTokenURL + "?UID=" + "")
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    expect(error.response.data).to.have.property("calToken");
                    expect(error.response.data.calToken).to.equal("");
                }).then(function () {
                    done();
            });
        });
    });
    

    /*
     * Tests: getTeams GET request
     *          - Correct client input for users with and without teams
     *          - Incorrect client input
     */
    describe("getTeams", function() {
        it("Returns {found:true, TIDs:[]} if UID is valid and in database without teams", function(done) {
            axios.get(getTeamsURL + "?UID=" + randomUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    expect(response.data.teams.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns {found:true, TIDs:2D array} if UID is valid and in database with teams", function(done) {
            axios.get(getTeamsURL + "?UID=" + existingUID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    expect(response.data.teams.length).to.equal(3);
                    
                    var teams = response.data.teams;
                    var team0Found = false;
                    var team1Found = false;
                    var team2Found = false;
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === existingTeam0[0] && team[1] === existingTeam0[1]){
                            team0Found = true;
                        }
                        if(team[0] === existingTeam1[0] && team[1] === existingTeam1[1]){
                            team1Found = true;
                        }
                        if(team[0] === existingTeam2[0] && team[1] === existingTeam2[1]){
                            team2Found = true;
                        }
                    }
                    
                    expect(team0Found && team1Found && team2Found).to.equal(true);
                    
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns {found:false, TIDs:[]} if UID is valid and not in database", function(done) {
            axios.get(getTeamsURL + "?UID=" + randomUID + "12")
                .then(function (response){
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(false);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.have.equal(Array);
                    expect(response.data.teams.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });

        it("Fails with status code 400 without UID", function(done) {
            axios.get(getTeamsURL)
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("found");
                    expect(error.response.data.found).to.equal(false);
                    
                    expect(error.response.data).to.have.property("teams");
                    expect(error.response.data.teams.constructor).to.have.equal(Array);
                    expect(error.response.data.teams.length).to.equal(0);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (empty string)", function(done) {
            axios.get(getTeamsURL+ "?UID=" + "")
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("found");
                    expect(error.response.data.found).to.equal(false);
                    
                    expect(error.response.data).to.have.property("teams");
                    expect(error.response.data.teams.constructor).to.have.equal(Array);
                    expect(error.response.data.teams.length).to.equal(0);
                }).then(function () {
                    done();
            });
        });
    });
    
    /*
     * Tests: addUserToTeam PUT request
     *          - Correct client input for users with no teams and with several teams
     *          - Incorrect client input
     *          - Multiple calls at the same time with same UID and TID
     */
    describe("addUserToTeam", function() {
        it("Adds UID without any teams to TID", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": randomUID,
                "TID": newTeam[0],
                "teamName": newTeam[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function(){
                    done();
            });
        });
        
        it("Returns that UID is now a member of TID", function(done){
             // confirms UID is now a member of team - assumes getTeams is working correctly
            axios.get(getTeamsURL + "?UID=" + randomUID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    expect(response.data.teams.length).to.equal(1);
                    
                    var team = response.data.teams[0];
                    expect(team[0]).to.equal(newTeam[0]);
                    expect(team[1]).to.equal(newTeam[1]);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Adds UID with teams to new TID", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": existingUID,
                "TID": newTeam[0],
                "teamName": newTeam[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function(){
                    done();
            });
        });
        
        it("Returns that UID is now a member of TID", function(done){
            axios.get(getTeamsURL + "?UID=" + existingUID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    
                    expect(response.data.teams.length).to.equal(4);
                    
                    var teams = response.data.teams;
                    var team0Found = false;
                    var team1Found = false;
                    var team2Found = false;
                    var newTeamFound = false;
                    for( var i = 0; i < teams.length; i++){
                        var team = teams[i];
                        expect(team.length).to.equal(2);
                        
                        expect(typeof team[0]).to.equal("string");
                        expect(typeof team[1]).to.equal("string");
                        
                        if(team[0] === existingTeam0[0] && team[1] === existingTeam0[1]){
                            team0Found = true;
                        }
                        if(team[0] === existingTeam1[0] && team[1] === existingTeam1[1]){
                            team1Found = true;
                        }
                        if(team[0] === existingTeam2[0] && team[1] === existingTeam2[1]){
                            team2Found = true;
                        }
                        if(team[0] === newTeam[0] && team[1] === newTeam[1]){
                            newTeamFound = true;
                        }
                    }
                    
                    expect(team0Found && team1Found && team2Found).to.equal(true);
                    expect(newTeamFound).to.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        
        it("Doesn't add UID to TID if already a member", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": randomUID,
                "TID": newTeam[0],
                "teamName": newTeam[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function(){
                    done();
            });
        });
        
        it("Fails with a user that does not exist", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": "validbutnotauser",
                "TID": newTeam[0],
                "teamName": newTeam[1]
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns that UID is still a member of TID", function(done){
             // confirms UID is not a member of team twice
            axios.get(getTeamsURL + "?UID=" + randomUID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    expect(response.data.teams.length).to.equal(1);
                    
                    var team = response.data.teams[0];
                    expect(team[0]).to.equal(newTeam[0]);
                    expect(team[1]).to.equal(newTeam[1]);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });

        
        it("Fails with status code 400 without UID", function(done) {
            axios.put(addUserToTeamURL, {
                "TID": "This is a valid TID",
                "teamName": "This is a valid Team Name"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without TID", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": "This is a valid UID",
                "teamName": "This is a valid Team Name"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without Team Name", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": "This is a valid UID",
                "TID": "This is a valid TID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (int)", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": 0,
                "TID": "This is a valid TID",
                "teamName": "This is a valid Team Name"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid teamName (bool)", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": "This is a valid UID",
                "TID": "This is a valid TID",
                "teamName": true
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (empty string)", function(done) {
            axios.put(addUserToTeamURL, {
                "UID": "",
                "TID": "This is a valid TID",
                "teamName": "This is a valid Team Name"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds if called with same UID and TID at the same time", function(done) {
            var firstCallUpdated = false;
            var secondCallUpdated = false;
            
            setTimeout(function(){
                axios.put(addUserToTeamURL, {
                    "TID": existingTeam0[0],
                    "UID": randomUID2,
                    "teamName": existingTeam0[1]
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        firstCallUpdated = response.data.updated;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                        
                });
            }, 500);
            
            setTimeout(function(){
                axios.put(addUserToTeamURL, {
                    "TID": existingTeam0[0],
                    "UID": randomUID2,
                    "teamName" : existingTeam0[1]
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        secondCallUpdated = response.data.updated;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                });
            }, 500);
            
            setTimeout(function(){
                // one of the two function calls should have been a new user
                expect(firstCallUpdated || secondCallUpdated).to.equal(true);
                expect(firstCallUpdated && secondCallUpdated).to.equal(false);
                done();
            }, 3000);
        });
        
        it("Confirms that UID is now a member of TID", function(done){
            // confirms user 1 is now a user, assumes getTeams is working correctly
                axios.get(getTeamsURL + "?UID=" + randomUID2)
                    .then(function (response) {
                        expect(response).to.have.property("status");
                        expect(response.status).to.equal(200);
                        
                        expect(response.data).to.have.property("found");
                        expect(response.data.found).to.have.equal(true);
                        
                        expect(response.data).to.have.property("teams");
                        expect(response.data.teams.constructor).to.equal(Array);
                        
                        expect(response.data.teams.length).to.equal(1);
                        
                        var team = response.data.teams[0];
                        expect(team[0]).to.equal(existingTeam0[0]);
                        expect(team[1]).to.equal(existingTeam0[1]);
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                    }).then(function () {
                        done();
                });
        });
    });


    /*
     * Tests: removeUserFromTeam PUT request
     *          - Correct client input for users with no teams, one team, and several teams
     *          - Incorrect client input
     *          - Multiple calls at the same time with same UID and TID
     */
    describe("removeUserFromTeam", function() {
        it("Removes UID with one team", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": randomUID,
                "TID": newTeam[0]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + randomUID)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            
                            expect(response.data.teams.length).to.equal(0);
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1); 
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Removes UID with several teams", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": existingUID,
                "TID": newTeam[0]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + existingUID)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            
                            expect(response.data.teams.length).to.equal(3);
                            
                            var teams = response.data.teams;
                            var team0Found = false;
                            var team1Found = false;
                            var team2Found = false;
                            var newTeamFound = false;
                            for( var i = 0; i < teams.length; i++){
                                var team = teams[i];
                                expect(team.length).to.equal(2);
                                
                                expect(typeof team[0]).to.equal("string");
                                expect(typeof team[1]).to.equal("string");
                                
                                if(team[0] === existingTeam0[0] && team[1] === existingTeam0[1]){
                                    team0Found = true;
                                }
                                if(team[0] === existingTeam1[0] && team[1] === existingTeam1[1]){
                                    team1Found = true;
                                }
                                if(team[0] === existingTeam2[0] && team[1] === existingTeam2[1]){
                                    team2Found = true;
                                }
                                if(team[0] === newTeam[0] && team[1] === newTeam[1]){
                                    newTeamFound = true;
                                }
                            }
                            
                            expect(team0Found && team1Found && team2Found).to.equal(true);
                            expect(newTeamFound).to.equal(false);
                            
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1); 
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Doesn't remove UID with from team it has already been removed.", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": existingUID,
                "TID": newTeam[0]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + existingUID)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            
                            expect(response.data.teams.length).to.equal(3);
                            
                            var teams = response.data.teams;
                            var team0Found = false;
                            var team1Found = false;
                            var team2Found = false;
                            var newTeamFound = false;
                            for( var i = 0; i < teams.length; i++){
                                var team = teams[i];
                                expect(team.length).to.equal(2);
                                
                                expect(typeof team[0]).to.equal("string");
                                expect(typeof team[1]).to.equal("string");
                                
                                if(team[0] === existingTeam0[0] && team[1] === existingTeam0[1]){
                                    team0Found = true;
                                }
                                if(team[0] === existingTeam1[0] && team[1] === existingTeam1[1]){
                                    team1Found = true;
                                }
                                if(team[0] === existingTeam2[0] && team[1] === existingTeam2[1]){
                                    team2Found = true;
                                }
                                if(team[0] === newTeam[0] && team[1] === newTeam[1]){
                                    newTeamFound = true;
                                }
                            }
                            
                            expect(team0Found && team1Found && team2Found).to.equal(true);
                            expect(newTeamFound).to.equal(false);
                            
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1); 
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Doesn't remove UID with no teams", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": randomUID,
                "TID": newTeam[0]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + randomUID)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            
                            expect(response.data.teams.length).to.equal(0);
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1); 
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        
        it("Fails with status code 400 without UID", function(done) {
            axios.put(removeUserFromTeamURL, {
                "TID": "This is a valid TID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with non-existing User", function(done) {
            axios.put(removeUserFromTeamURL, {
                "TID": "This is a valid TID",
                "UID": "thisusernoexist"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without TID", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": "This is a valid UID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (int)", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": 0,
                "TID": "This is a valid TID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid TID (json object)", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": "This is a valid UID",
                "TID": {"This is an invalid TID":"cause it is a json object"}
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds if called with same UID and TID at the same time", function(done) {
            var firstCallUpdated = false;
            var secondCallUpdated = false;
            
            setTimeout(function(){
                axios.put(removeUserFromTeamURL, {
                    "TID": existingTeam0[0],
                    "UID": randomUID2
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        firstCallUpdated = response.data.updated;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                        
                });
            }, 500);
            
            setTimeout(function(){
                axios.put(removeUserFromTeamURL, {
                    "TID": existingTeam0[0],
                    "UID": randomUID2
                    }).then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        secondCallUpdated = response.data.updated;
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                });
            }, 500);
            
            setTimeout(function(){
                // one of the two function calls should have been an update
                expect(firstCallUpdated || secondCallUpdated).to.equal(true);
                expect(firstCallUpdated && secondCallUpdated).to.equal(false);
                
                // confirms user 1 is now a user, assumes getTeams is working correctly
                axios.get(getTeamsURL + "?UID=" + randomUID2)
                    .then(function (response) {
                        expect(response).to.have.property("status");
                        expect(response.status).to.equal(200);
                        
                        expect(response.data).to.have.property("found");
                        expect(response.data.found).to.have.equal(true);
                        
                        expect(response.data).to.have.property("teams");
                        expect(response.data.teams.constructor).to.equal(Array);
                        
                        expect(response.data.teams.length).to.equal(0);
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                    }).then(function () {
                        done();
                });
            }, 3000);
        });
    });

    /*
     * Tests: deleteUser DELETE request
     *          - Correct client input
     *          - Incorrect client input
     *          - Multiple calls at the same time with same UID
     */
    describe("deleteUser", function() {
        var deleteUserURL = userURL + "deleteUser";
        var getCalTokenURL = userURL + "getCalToken";
        var getTeamsURL = userURL + "getTeams";
        
        it("Returns {updated:true} with valid UID present in database", function(done) {
            axios.delete(deleteUserURL + "?UID=" + randomUID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is not a member of team - assumes getCalToken is working correctly
                    axios.get(getCalTokenURL + "?UID=" + randomUID)
                        .then(function (response) {
                            // should not be reached
                            expect(1).to.equal(-1); 
                        }).catch(function (error) {
                            expect(error).to.have.property("response");
                            expect(error.response.status).to.equal(400);
                            expect(error.response.data).to.have.property("errMessage");
                            expect(error.response.data).to.have.property("errCode");
                            expect(error.response.data.errCode).to.equal(400);
                            expect(error.response.data).to.have.property("calToken");
                            expect(error.response.data.calToken).to.equal("");
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Returns {updated:false} user already deleted", function(done) {
            axios.delete(deleteUserURL + "?UID=" + randomUID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                    
                    // confirms UID is not a member of team - assumes getCalToken is working correctly
                    axios.get(getCalTokenURL + "?UID=" + randomUID)
                        .then(function (response) {
                            // should not be reached
                            expect(1).to.equal(-1); 
                        }).catch(function (error) {
                            expect(error).to.have.property("response");
                            expect(error.response.status).to.equal(400);
                            expect(error.response.data).to.have.property("errMessage");
                            expect(error.response.data).to.have.property("errCode");
                            expect(error.response.data.errCode).to.equal(400);
                            expect(error.response.data).to.have.property("calToken");
                            expect(error.response.data.calToken).to.equal("");
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Returns {updated:false} with valid UID not present in database", function(done) {
            axios.delete(deleteUserURL + "?UID=" + randomUID + "22")
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                    
                    // confirms UID is not a member of team - assumes getCalToken is working correctly
                    axios.get(getCalTokenURL + "?UID=" + randomUID + "22")
                        .then(function (response) {
                            // should not be reached
                            expect(1).to.equal(-1); 
                        }).catch(function (error) {
                            expect(error).to.have.property("response");
                            expect(error.response.status).to.equal(400);
                            expect(error.response.data).to.have.property("errMessage");
                            expect(error.response.data).to.have.property("errCode");
                            expect(error.response.data.errCode).to.equal(400);
                            expect(error.response.data).to.have.property("calToken");
                            expect(error.response.data.calToken).to.equal("");
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Fails with status code 400 without UID", function(done) {
            axios.delete(deleteUserURL)
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid UID (empty string)", function(done) {
            axios.delete(deleteUserURL + "?UID=" + "")
                .then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                    
                    expect(error.response.data).to.have.property("updated");
                    expect(error.response.data.updated).to.equal(false);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds if called with same UID at the same time", function(done) {
            var firstCallUpdated = false;
            var secondCallUpdated = false;
            
            setTimeout(function(){
                axios.delete(deleteUserURL + "?UID=" + randomUID2)
                    .then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        firstCallUpdated = response.data.updated;
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                        
                });
            }, 500);
            
            setTimeout(function(){
                axios.delete(deleteUserURL + "?UID=" + randomUID2)
                    .then(function (response) {
                        expect(response.status).to.equal(200);
                        expect(response.data).to.have.property("updated");
                        secondCallUpdated = response.data.updated;
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1); 
                });
            }, 500);
            
            setTimeout(function(){
                // one of the two function calls should have been deleted the user
                expect(firstCallUpdated || secondCallUpdated).to.equal(true);
                
                // confirms user 1 is now a user, assumes getTeams is working correctly
                axios.get(getTeamsURL + "?UID=" + randomUID2)
                    .then(function (response) {
                        expect(response).to.have.property("status");
                        expect(response.status).to.equal(200);
                        
                        expect(response.data).to.have.property("found");
                        expect(response.data.found).to.have.equal(false);
                        
                        expect(response.data).to.have.property("teams");
                        expect(response.data.teams.constructor).to.equal(Array);
                        
                        expect(response.data.teams.length).to.equal(0);
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                    }).then(function () {
                        done();
                });
            }, 3000);
        });
    });
    
    describe("Expected usecase", function() {
        var userID1 = Math.random().toString();
        var userToken1 = Math.random().toString();
        
        var userID2 = Math.random().toString();
        var userToken2 = Math.random().toString();
        
        var sharedTeamID = Math.random().toString();
        var sharedTeamName = Math.random().toString();
        
        
        it("Allows user1 to log in successfully", function(done) {
            axios.post(newLoginURL, {
                "displayName": "User1",
                "UID": userID1,
                "calToken": userToken1
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("new");
                    expect(response.data.new).to.have.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user2 to log in successfully", function(done) {
            axios.post(newLoginURL, {
                "displayName": "User2",
                "UID": userID2,
                "calToken": userToken2
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("new");
                    expect(response.data.new).to.have.equal(true);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("No teams returned for user1, but they are found in the database", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID1, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    expect(response.data.teams.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("No teams returned for user2, but they are found in the database", function(done) {
            axios.get(getTeamsURL + "?UID=" + userID2, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.have.equal(true);
                    
                    expect(response.data).to.have.property("teams");
                    expect(response.data.teams.constructor).to.equal(Array);
                    expect(response.data.teams.length).to.equal(0);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user1 and user2 to join shared team at same time", function(done) {
            setTimeout(function(){
                axios.put(addUserToTeamURL, {
                "UID": userID1,
                "TID": sharedTeamID,
                "teamName": sharedTeamName
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + userID1)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            expect(response.data.teams.length).to.equal(1);
                            
                            var team = response.data.teams[0];
                            expect(team[0]).to.equal(sharedTeamID);
                            expect(team[1]).to.equal(sharedTeamName);
                            
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1);
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
            }, 500);
            
            setTimeout(function(){
                axios.put(addUserToTeamURL, {
                "UID": userID2,
                "TID": sharedTeamID,
                "teamName": sharedTeamName
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + userID2)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            expect(response.data.teams.length).to.equal(1);
                            
                            var team = response.data.teams[0];
                            expect(team[0]).to.equal(sharedTeamID);
                            expect(team[1]).to.equal(sharedTeamName);
                            
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1);
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
            }, 500);
            
            setTimeout(function(){
                done();
            }, 3000);
        });
        
        it("Succeeds if user1 accidentally tries to join the same team at same time", function(done) {
            var firstCallUpdated = false;
            var secondCallUpdated = false;
            
            setTimeout(function(){
                axios.put(addUserToTeamURL, {
                "UID": userID1,
                "TID": newTeam[0],
                "teamName": newTeam[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    
                    firstCallUpdated = response.data.updated;
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
            }, 500);
            
            setTimeout(function(){
                axios.put(addUserToTeamURL, {
                "UID": userID1,
                "TID": newTeam[0],
                "teamName": newTeam[1]
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    
                    secondCallUpdated = response.data.updated;
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
            }, 500);
            
            setTimeout(function(){
                // only one of the two function calls should have been successful
                expect(firstCallUpdated || secondCallUpdated).to.equal(true);
                expect(firstCallUpdated && secondCallUpdated).to.equal(false);
                
                // confirms user 1 is now a member of team
                axios.get(getTeamsURL + "?UID=" + userID1)
                    .then(function (response) {
                        expect(response).to.have.property("status");
                        expect(response.status).to.equal(200);
                        
                        expect(response.data).to.have.property("found");
                        expect(response.data.found).to.have.equal(true);
                        
                        expect(response.data).to.have.property("teams");
                        expect(response.data.teams.constructor).to.equal(Array);
                        
                        expect(response.data.teams.length).to.equal(2);
                        
                        var teams = response.data.teams;
                        var sharedTeamFound = false;
                        var newTeamFound = false;
                        for( var i = 0; i < teams.length; i++){
                            var team = teams[i];
                            expect(team.length).to.equal(2);
                            
                            expect(typeof team[0]).to.equal("string");
                            expect(typeof team[1]).to.equal("string");
                            
                            if(team[0] === sharedTeamID && team[1] === sharedTeamName){
                                sharedTeamFound = true;
                            }
                            if(team[0] === newTeam[0] && team[1] === newTeam[1]){
                                newTeamFound = true;
                            }
                        }
                        
                        expect(sharedTeamFound).to.equal(true);
                        expect(newTeamFound).to.equal(true);
                        
                    }).catch(function (error) {
                        console.log(error); // should not be reached
                        expect(1).to.equal(-1);
                });
                done();
            }, 3000);
        });
        
        it("Fetches calToken for user1", function(done) {
            axios.get(getCalTokenURL + "?UID=" + userID1)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("calToken");
                    expect(response.data.calToken).to.have.equal(userToken1);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fetches calToken for user2", function(done) {
            axios.get(getCalTokenURL + "?UID=" + userID2)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("calToken");
                    expect(response.data.calToken).to.have.equal(userToken2);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user2 to remove themselves from the team", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": userID2,
                "TID": sharedTeamID
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + userID2)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            
                            expect(response.data.teams.length).to.equal(0);
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1); 
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Allows user1 to remove themselves from the team", function(done) {
            axios.put(removeUserFromTeamURL, {
                "UID": userID1,
                "TID": sharedTeamID
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is now a member of team - assumes getTeams is working correctly
                    axios.get(getTeamsURL + "?UID=" + userID1)
                        .then(function (response) {
                            expect(response).to.have.property("status");
                            expect(response.status).to.equal(200);
                            
                            expect(response.data).to.have.property("found");
                            expect(response.data.found).to.have.equal(true);
                            
                            expect(response.data).to.have.property("teams");
                            expect(response.data.teams.constructor).to.equal(Array);
                            
                            expect(response.data.teams.length).to.equal(1);
                            var team = response.data.teams[0];
                            expect(team[0]).to.equal(newTeam[0]);
                            expect(team[1]).to.equal(newTeam[1]);
                        }).catch(function (error) {
                            console.log(error); // should not be reached
                            expect(1).to.equal(-1); 
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Allows user1 to delete themselves from the database", function(done) {
            axios.delete(deleteUserURL + "?UID=" + userID1)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is not a member of team - assumes getCalToken is working correctly
                    axios.get(getCalTokenURL + "?UID=" + userID1)
                        .then(function (response) {
                            // should not be reached
                            expect(1).to.equal(-1); 
                        }).catch(function (error) {
                            expect(error).to.have.property("response");
                            expect(error.response.status).to.equal(400);
                            expect(error.response.data).to.have.property("errMessage");
                            expect(error.response.data).to.have.property("errCode");
                            expect(error.response.data.errCode).to.equal(400);
                            expect(error.response.data).to.have.property("calToken");
                            expect(error.response.data.calToken).to.equal("");
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
        it("Allows user2 to delete themselves from the database", function(done) {
            axios.delete(deleteUserURL + "?UID=" + userID2)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                    
                    // confirms UID is not a member of team - assumes getCalToken is working correctly
                    axios.get(getCalTokenURL + "?UID=" + userID2)
                        .then(function (response) {
                            // should not be reached
                            expect(1).to.equal(-1); 
                        }).catch(function (error) {
                            expect(error).to.have.property("response");
                            expect(error.response.status).to.equal(400);
                            expect(error.response.data).to.have.property("errMessage");
                            expect(error.response.data).to.have.property("errCode");
                            expect(error.response.data.errCode).to.equal(400);
                            expect(error.response.data).to.have.property("calToken");
                            expect(error.response.data.calToken).to.equal("");
                        }).then(function () {
                            done();
                    });
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
            });
        });
        
    });
});