var im = require('istanbul-middleware');
var isCoverageEnabled = (true);

var express = require("express");
var path = require("path");
var app = express();
var firebase = require("firebase");
var axios = require("axios");
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var admin = require("firebase-admin");
var globMeta = require(__dirname);
var fs = require('fs');

var db = {};

// Load service account secrets from a local file.
fs.readFile('/app/server/firestoreadmin.json', (err, content) => {
    console.log(__dirname);
    
    if (err) return console.log('Error loading firestore admin file:', err);
  
    const serviceAccount = JSON.parse(content);
    
    // Initialize Firebase
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        apiKey: "AIzaSyDYD4WI1hRgBOxM5IOs3YEx0T0NtlXMuqk",
        authDomain: "cpen321-users.firebaseapp.com",
        databaseURL: "https://cpen321-users.firebaseio.com",
        projectId: "cpen321-users",
        storageBucket: "cpen321-users.appspot.com",
        messagingSenderId: "285954961097"
    });
    
    // Get a reference to the firestore service
    db = admin.firestore();
});

var utils = require("utils");

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

module.exports = {
    start: function(){
        app.set("port", (process.env.PORT || 5000));

        app.use(express.json());
        app.use(express.urlencoded());
        
        // for testing only - remove if deployed
        app.use('/coverage', im.createHandler());

        app.use(function(req,res,next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type,Accept");
            next();
        });
        
        // allows access from local host
        app.use("/", express.static(path.join(__dirname, "..")));


        /*==============================================
         * GET, POST, PUT requests
         *==============================================*/

        /*
         * POST request endpoint which checks to see if a user is the the database, adds them if they are not
         *
         * query requirements:  None
         * body requirements:   - UID : string
         *                      - displayName : string
         *
         * returned JSON:       - new : bool; true if user was not present in database and was added
         * 
         * returns error if requirements are not correct type or are not present in request body, "new" not present in JSON on error
         */
        app.post("/newLogin", function(myRequest, myResponse){
            
            var userID = myRequest.body.UID;
            var displayName = myRequest.body.displayName;
            var myErrorMessage = "";
            
            if(utils.checkIfUndefined([userID, displayName])){
                myErrorMessage = "400 Error: Expected newLogin POST request to contain UID and displayName";
                utils.returnError(myResponse, myErrorMessage, 400);
            }
            else if(!utils.checkIfStrings([userID, displayName])){
                myErrorMessage = "400 Error: Expected newLogin POST request UID and displayName to be strings";
                utils.returnError(myResponse, myErrorMessage, 400);
            }    
            else{
                console.log(userID);
                var docRef = db.collection("Users").doc(userID);
                
                db.runTransaction(function(transaction){
                    return transaction.get(docRef).then(function(doc){
                        if(!doc.exists){
                            transaction.set(docRef, {
                                displayName,
                                TIDs:[],
                                teamNames:[]
                            });
                            
                            console.log("Added new user to our database.  Welcome aboard " + displayName + "!");
                            return true;
                            
                        } else{
                            transaction.update(docRef, {
                                displayName
                            });
                            
                            console.log("Welcome back " + displayName + "!");
                            return false;
                        }
                    });
                }).then(function(isNewUser){
                    myResponse.status(200);                    
                    myResponse.json({"new":isNewUser});
                    myResponse.end();
                })
                .catch(function(error){
                    myErrorMessage = "500 Error: Unable to perform newLogin; error returned: " + error;
                    utils.returnError(myResponse, myErrorMessage, 500);
                });
            }    
        });

        /*
         * GET request endpoint which returns the calendar oauth token of the given user
         *
         * query requirements:  - UID : string
         * body requirements:   None
         *
         * returned JSON:       - calToken : string
         * 
         * returns error if UID not present in database or not present in query, calToken is an empty string on error 
         */
         app.get("/getCalToken", function(myRequest, myResponse) {
            var userID = myRequest.query["UID"];
            var myErrorMessage = "";
            
            if(typeof userID !== "string" || userID.length <= 0){
                myErrorMessage = "400 Error: Expected string UID in query of getCalToken GET request.";
                utils.returnError(myResponse, myErrorMessage, 400, {"calToken":""});
            } 
            else{
                var docRef = db.collection("Users").doc(userID);
                
                docRef.get().then(function(doc){
                    if(!doc.exists){
                        myErrorMessage = "400 Error: Expected UID to be present in the database";
                        utils.returnError(myResponse, myErrorMessage, 400, {"calToken":""});
                        
                    } else{
                        myResponse.status(200);
                        console.log("UID " + userID + " is valid, returned token");
                        myResponse.json({"calToken":doc.data().calToken});
                        myResponse.end();
                    }
                });
            }
            
        });

        /*
         * GET request endpoint which returns the TIDs the given user
         *
         * query requirements:  - UID : string
         * body requirements:   None
         *
         * returned JSON:       - found : bool; true if user is present in the database
         *                      - teams : 2D array of strings; empty array if user is not present in the database
         *
         * returns error if UID is not present in query, found is false and teams is an empty array on error
         */
         app.get("/getTeams", function(myRequest, myResponse) {
            var userID = myRequest.query["UID"];
            
            if(typeof userID !== "string" || userID.length <= 0){
                var myErrorMessage = "400 Error: Expected UID in getTeams";
                utils.returnError(myResponse, myErrorMessage, 400, {"found":false, "teams":[]});
            } 
            else{
                var docRef = db.collection("Users").doc(userID);
                
                docRef.get().then(function(doc){
                    
                    myResponse.status(200);
                    
                    if(!doc.exists){
                        console.log("UID " + userID + " is invalid, returned empty teams");
                        myResponse.json({"found":false, "teams":[]});
                        myResponse.end();
                        
                    } else{
                        var TIDs = doc.data().TIDs;
                        var teamNames = doc.data().teamNames;
                        
                        var combinedArray = [];
                        
                        while(TIDs.length > 0){
                            combinedArray.push([TIDs.pop(), teamNames.pop()]);
                        }
                        
                        console.log("UID " + userID + " is valid, returned teams");
                        myResponse.json({"found":true, "teams":combinedArray});
                        
                        myResponse.end();
                    }
                });
            }
            
        });

        /*
         * PUT request endpoint which adds the user to a given team
         *
         * query requirements:  None
         * body requirements:   - UID : string
         *                      - TID : string
         *                      - teamName : string
         *
         * returned JSON:       - updated : bool; true if user was present in database and was updated
         *                                  user can not belong to the same team twice - if UID is already
         *                                  a member of team with TID, they are not updated.          
         *
         * returns error if UID, TID, or teamName are not present in body as strings, updated false on error
         */
        app.put("/addUserToTeam", function(myRequest, myResponse) {

            var userID = myRequest.body.UID;
            var teamID = myRequest.body.TID;
            var teamName = myRequest.body.teamName;
            
            var myErrorMessage = "";
            
            if(utils.checkIfUndefined([userID, teamID, teamName])){
                myErrorMessage = "400 Error: Expected UID, TID, and teamName in addUserToTeam";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else if(!utils.checkIfStrings([userID, teamID, teamName])){
                myErrorMessage = "400 Error: Expected UID, TID, and teamName to be strings in addUserToTeam";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var docRef = db.collection("Users").doc(userID);
                
                db.runTransaction(function(transaction) {
                    return transaction.get(docRef).then(function(doc) {
                        if(!doc.exists) {
                            throw 400;
                        }
                        var currentTIDs = doc.data().TIDs;
                        
                        // check if user already a member
                        var member = false;
                        for(var i = 0; i < currentTIDs.length; i++){
                            if(currentTIDs[i] === teamID){
                                member = true;
                                break;
                            }
                        }
                        if(member){
                            return false;
                        }
                        else{
                            currentTIDs.push(teamID);
                            
                            
                            var currentTeamNames = doc.data().teamNames;
                            currentTeamNames.push(teamName);
                            
                            transaction.update(docRef, {
                                TIDs: currentTIDs,
                                teamNames: currentTeamNames
                            });
                            
                            return true;
                        }
                    });
                }).then(function(updated) {
                    console.log("UID " + userID + " is a part of TID " + teamID);
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                    
                }).catch(function(error) {
                    if(error === 400){
                        myErrorMessage = "400 Error: UID " + userID + " not found.";
                        utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                    }
                    else{
                        myErrorMessage = "500 Error: Unable to add user to team, error returned: " + error;
                        utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                    }
                });
            }
        });

        /*
         * PUT request endpoint which removes the user to a given team
         *
         * query requirements:  None
         * body requirements:   - UID : string
         *                      - TID : string
         *
         * returned JSON:       - updated : bool; true if user was present in database and was updated
         *                                  if UID is already not a member of team with TID, they are not updated.          
         *
         * returns error if UID or TID are not present in body as strings, updated false on error
         */
        app.put("/removeUserFromTeam", function(myRequest, myResponse) {

            var userID = myRequest.body.UID;
            var teamID = myRequest.body.TID;
            
            var myErrorMessage = "";
            
            if(utils.checkIfUndefined([userID, teamID])){
                myErrorMessage = "400 Error: Expected UID, TID, and teamName in removeUserFromTeam";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else if(!utils.checkIfStrings([userID, teamID])){
                myErrorMessage = "400 Error: Expected UID, TID, and teamName to be strings in removeUserFromTeam";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var docRef = db.collection("Users").doc(userID);
                
                db.runTransaction(function(transaction) {
                    return transaction.get(docRef).then(function(doc) {
                        if(!doc.exists) {
                            throw 400;
                        }

                        var currentTIDs = doc.data().TIDs;
                        
                        // check if user already a member
                        var index = currentTIDs.indexOf(teamID);
                        if(index > -1){
                            // user was a member of TID
                            var currentTeamNames = doc.data().teamNames;
                            
                            currentTeamNames.splice(index, 1);
                            currentTIDs.splice(index, 1);
                            
                            transaction.update(docRef, {
                                TIDs: currentTIDs,
                                teamNames: currentTeamNames
                            });
                            return true;
                            
                        }
                        else{
                            return false;
                        }
                    });
                }).then(function(updated) {
                    console.log("UID " + userID + " is not a part of TID " + teamID);
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                }).catch(function(error) {
                    if(error == 400){
                        myErrorMessage = "400 Error: UID " + userID + " not found.";
                        utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                    }
                    else{
                        myErrorMessage = "500 Error: Unable to remove user from team, error returned: " + error;
                        utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                    }
                });
            }
        });

        /*
         * DELETE request endpoint which removes the user to a given team
         *
         * query requirements:  - UID : string
         * body requirements:   None
         *
         * returned JSON:       - updated : bool; true if user was present in database and was deleted
         *                                  if UID is not found in database, they are not updated.          
         *
         * returns error if UID is not present in query, updated false on error
         */
        app.delete("/deleteUser", function(myRequest, myResponse) {
            var userID = myRequest.query.UID;
            
            var myErrorMessage = "";
            
            if(typeof userID !== "string" || userID.length <= 0){
                myErrorMessage = "400 Error: Expected UID in deleteUser as a string";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var docRef = db.collection("Users").doc(userID);
                
                return db.runTransaction(function(transaction) {
                    return transaction.get(docRef).then(function(doc) {
                        if(!doc.exists) {
                            return false;
                        }
                        
                        transaction.delete(docRef);
                        return true;
                    });
                }).then(function(updated) {
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                }).catch(function(error) {
                    myErrorMessage = "500 Error: Unable to delete user, error returned: " + error;
                    utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                });
            }
            
        });

        app.listen(app.get("port"), function() {
          console.log("Node app is running at localhost:" + app.get("port"));
        });

    }
};