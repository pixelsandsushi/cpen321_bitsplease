var expect  = require("chai").expect;
var axios = require("axios");

var votingURL = "https://bitsplease-voting-service.herokuapp.com/";
var newPollURL = votingURL + "newPoll";
var closePollURL = votingURL + "closePoll";
var getVotesURL = votingURL + "getVotes";
var voteURL = votingURL + "vote";
var getActivePollsURL = votingURL + "getActivePolls";
var deletePollURL = votingURL + "deletePoll";

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

describe("Voting Service", function() {
    this.timeout(6000);
    
    var createdPID = "";
    
    // ensures server is in the correct state for testing
    describe("Server is in the right state", function(){
        it("Succeeds in waking up the server", function(done){
            this.timeout(50000);
            axios.get(votingURL)
                .then(function (response) {
                    console.log(response.status);
                }).catch(function (error) {
                    expect(error.response.status).to.equal(404);
                }).then(function () {
                    done();
            });
        });
    });
    
    /*
     * Tests: newPoll POST request
     *          - Correct client input for team with polls
     *          - Correct client input for team without polls
     *          - Incorrect client input (invalid variables)
     *          - Multiple calls at the same time with same TID
     */
    describe("newPoll", function() {
        it("Succeds if given basic details", function(done) {
            axios.post(newPollURL, {
                "options": [1, 2, 3, 4],
                "MID": "meetupID",
                "TID": "newTeam",
                "meetupName":"meetupName"
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("PID");
                    expect(typeof response.data.PID).to.equal("string");
                    
                    createdPID = response.data.PID
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 without TID", function(done) {
            axios.post(newPollURL, {
                "options": [1, 2, 3, 4],
                "MID":"meetupID",
                "meetupName":"meetupName"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid MID (int)", function(done) {
            axios.post(newPollURL, {
                "options": [1, 2, 3, 4],
                "MID":1,
                "meetupName":"meetupName",
                "TID":"teamID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid options (string)", function(done) {
            axios.post(newPollURL, {
                "options": "test",
                "MID":"meetupID",
                "meetupName":"meetupName",
                "TID":"teamID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
    });
    
    /*
     * Tests: vote PUT request
     *          - Correct client input for existing open poll with votes, user has not voted
     *          - Correct client input for existing open poll with votes, user has voted
     *          - Correct client input for existing open poll without votes
     *          - Correct client input for existing closed poll
     *          - Incorrect client input (invalid variables)
     *          - Incorrect client input (PID does not exist)
     *          - Multiple calls at the same time with same PID, different users
     *          - Multiple calls at the same time with same PID and user
     */
    describe("vote", function() {
        it("Fails with status code 400 if Poll does not exist", function(done) {
            axios.put(voteURL + "?PID=" + "poll does not exisit", {
                "vote":0,
                "UID":"userID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if PID is invalid (empty string)", function(done) {
            axios.put(voteURL + "?PID=" + "", {
                "vote":0,
                "UID":"userID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if UID is invalid (empty string)", function(done) {
            axios.put(voteURL + "?PID=" + createdPID, {
                "vote":0,
                "UID":""
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if UID is not present", function(done) {
            axios.put(voteURL + "?PID=" + createdPID, {
                "vote":0
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if vote is out of bounds (too low)", function(done) {
            axios.put(voteURL + "?PID=" + createdPID, {
                "vote":-1,
                "UID":"userID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if vote is out of bounds (too high)", function(done) {
            axios.put(voteURL + "?PID=" + createdPID, {
                "vote":4,
                "UID":"userID"
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds with one vote", function(done) {
            axios.put(voteURL + "?PID=" + createdPID, {
                "vote":0,
                "UID":"userID1"
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true); 
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds with two votes", function(done) {
            axios.put(voteURL + "?PID=" + createdPID, {
                "vote":2,
                "UID":"userID2"
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true); 
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds with changed vote", function(done) {
            axios.put(voteURL + "?PID=" + createdPID, {
                "vote":1,
                "UID":"userID2"
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true); 
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
    });
    
    /*
     * Tests: closePoll PUT request
     *          - Correct client input existing open poll
     *          - Correct client input existing closed poll
     *          - Incorrect client input (invalid variables)
     *          - Incorrect client input (PID does not exist)
     *          - Multiple calls at the same time with same PID
     */
    describe("closePoll", function() {
        it("Succeeds if given an open poll", function(done) {
            axios.put(closePollURL + "?PID=" + createdPID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Succeeds if given an closed poll", function(done) {
            axios.put(closePollURL + "?PID=" + createdPID)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 with invalid PID (empty string)", function(done) {
            axios.put(closePollURL + "?PID=" + "", {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if Poll does not exist", function(done) {
            axios.put(closePollURL + "?PID=" + "poll does not exisit", {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
    });

    /*
     * Tests: getActivePolls GET request
     *          - Correct client input for existing team with active polls
     *          - Correct client input for existing team without any polls
     *          - Correct client input for existing team without active polls
     *          - Correct client input for non-existing team
     *          - Incorrect client input (invalid variables)
     */
    describe("getActivePolls", function() {
        it("Succeeds with valid TID", function(done) {
            axios.get(getActivePollsURL + "?TID=" + createdPID, {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("activePolls");
                    expect(response.data.activePolls.constructor).to.equal(Array);
                    expect(response.data.activePolls.length).to.equal(0);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if invalid PID (empty string)", function(done) {
            axios.get(getActivePollsURL + "?TID=" + "", {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
    });
    

    
    /*
     * Tests: getVotes GET request
     *          - Correct client input for existing poll with votes
     *          - Correct client input for existing poll without votes
     *          - Incorrect client input (invalid variables)
     *          - Incorrect client input (PID does not exist)
     */
    describe("getVotes", function() {
        it("Succeeds if PID is valid", function(done) {
            axios.get(getVotesURL + "?PID=" + createdPID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(4);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(i+1);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(voteCounts).to.equal(2);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if Poll does not exist", function(done) {
            axios.get(getVotesURL + "?PID=" + "poll does not exisit", {
                }).then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if invalid PID (empty string)", function(done) {
            axios.get(getVotesURL + "?PID=" + "", {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
        it("Fails with status code 400 if PID is not present", function(done) {
            axios.get(getVotesURL, {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });
        
    });
    
    /*
     * Tests: deletePoll DELETE request
     *          - Correct client input for existing poll
     *          - Correct client input for non-existing poll
     *          - Incorrect client input (invalid PID)
     *          - Multiple calls at the same time with same PID
     */
    describe("deletePoll", function() {
        it("Fails with status code 400 if PID is not present", function(done) {
            axios.delete(deletePollURL, {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });

        it("Fails with status code 400 if PID is invalid (empty string)", function(done) {
            axios.delete(deletePollURL + "?PID=" + "", {
                }).then(function (response) {
                    // should not be reached
                    expect(1).to.equal(-1); 
                }).catch(function (error) {
                    expect(error).to.have.property("response");
                    expect(error.response.status).to.equal(400);
                    expect(error.response.data).to.have.property("errMessage");
                    expect(error.response.data).to.have.property("errCode");
                    expect(error.response.data.errCode).to.equal(400);
                }).then(function () {
                    done();
            });
        });

        it("Succeeds in deleting team with exisiting PID", function(done) {
            axios.delete(deletePollURL + "?PID=" + createdPID, {
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });

        it("Succeeds in deleting team with already deleted PID", function(done) {
            axios.delete(deletePollURL + "?PID=" + createdPID, {
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });

        it("Succeeds in deleting team with PID that never existed", function(done) {
            axios.delete(deletePollURL + "?PID=" + "this PID is garbage", {
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });         
    });
    
    describe("Expected usecase", function() {
        var userID1 = Math.random().toString();
        var userID2 = Math.random().toString();
        var userID3 = Math.random().toString();
        
        var teamID = Math.random().toString();
        var meetupID = Math.random().toString();
        var meetupName = Math.random().toString();
        
        var pollID1 = undefined;
        var pollOptions1 = ["yes", "no"];
        var numOptions1 = 2;
        
        var pollID2 = undefined;
        var pollOptions2 = ["cats", "dogs", "other"];
        var numOptions2 = 3;
        
        
        it("Returns no active polls for team", function(done){
            axios.get(getActivePollsURL + "?TID=" + teamID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("activePolls");
                    expect(response.data.activePolls.constructor).to.equal(Array);
                    expect(response.data.activePolls.length).to.equal(0);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully creates first new poll for team", function(done){
            axios.post(newPollURL, {
                "options": pollOptions1,
                "TID":teamID,
                "MID":meetupID,
                meetupName
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("PID");
                    expect(typeof response.data.PID).to.equal("string");
                    
                    pollID1 = response.data.PID
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns no votes for newly created poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions1);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions1[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(voteCounts).to.equal(0);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns one active poll for team", function(done){
            axios.get(getActivePollsURL + "?TID=" + teamID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("activePolls");
                    expect(response.data.activePolls.constructor).to.equal(Array);
                    expect(response.data.activePolls.length).to.equal(1);
                    
                    var onePoll = response.data.activePolls[0];
                    
                    expect(onePoll.constructor).to.equal(Array);
                    expect(onePoll.length).to.equal(3);
                    
                    var onePollID = onePoll[0];
                    var onePollMeetupID = onePoll[1];
                    var onePollMeetupName = onePoll[2];
                    
                    expect(onePollID).to.equal(pollID1);
                    expect(onePollMeetupID).to.equal(meetupID);
                    expect(onePollMeetupName).to.equal(meetupName);
                    
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully creates a new poll for team", function(done){
            axios.post(newPollURL, {
                "options": pollOptions2,
                "TID":teamID,
                "MID":meetupID,
                meetupName
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("PID");
                    expect(typeof response.data.PID).to.equal("string");
                    
                    pollID2 = response.data.PID
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns no votes for newly created poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID2)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions2);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions2[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(voteCounts).to.equal(0);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns two active polls for team", function(done){
            axios.get(getActivePollsURL + "?TID=" + teamID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("activePolls");
                    
                    var activePolls = response.data.activePolls;
                    
                    expect(activePolls.constructor).to.equal(Array);
                    expect(activePolls.length).to.equal(2);
                    
                    var firstPollFound = false;
                    var secondPollFound = false;
                    
                    for(var i = 0; i < activePolls.length; i++){
                        var poll = activePolls[i];
                    
                        expect(poll.constructor).to.equal(Array);
                        expect(poll.length).to.equal(3);
                        
                        expect(poll[1]).to.equal(meetupID);
                        expect(poll[2]).to.equal(meetupName);
                        
                        if(poll[0] === pollID1){
                            firstPollFound = true;
                        }
                        
                        if(poll[0] === pollID2){
                            secondPollFound = true;
                        }
                    }
                    
                    expect(firstPollFound).to.equal(true);
                    expect(secondPollFound).to.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user1 to vote on first poll", function(done){
            axios.put(voteURL + "?PID=" + pollID1, {
                "vote" : 0,
                "UID" : userID1
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns one vote for first poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions1);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions1[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(votes[0][1]).to.equal(1);
                    
                    expect(voteCounts).to.equal(1);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user2 to vote on first poll", function(done){
            axios.put(voteURL + "?PID=" + pollID1, {
                "vote" : 0,
                "UID" : userID2
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user3 to vote on first poll", function(done){
            axios.put(voteURL + "?PID=" + pollID1, {
                "vote" : 1,
                "UID" : userID3
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns three votes for first poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions1);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions1[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(votes[0][1]).to.equal(2);
                    expect(votes[1][1]).to.equal(1);
                    
                    expect(voteCounts).to.equal(3);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user1 to change their mind on first poll", function(done){
            axios.put(voteURL + "?PID=" + pollID1, {
                "vote" : 1,
                "UID" : userID1
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns three votes for first poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions1);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions1[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(votes[0][1]).to.equal(1);
                    expect(votes[1][1]).to.equal(2);
                    
                    expect(voteCounts).to.equal(3);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user2 to vote on second poll", function(done){
            axios.put(voteURL + "?PID=" + pollID2, {
                "vote" : 2,
                "UID" : userID2
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user3 to vote on second poll", function(done){
            axios.put(voteURL + "?PID=" + pollID2, {
                "vote" : 0,
                "UID" : userID3
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns two votes for second poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID2)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions2);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions2[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(votes[0][1]).to.equal(1);
                    expect(votes[1][1]).to.equal(0);
                    expect(votes[2][1]).to.equal(1);
                    
                    expect(voteCounts).to.equal(2);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully closes second poll", function(done){
            axios.put(closePollURL + "?PID=" + pollID2)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns one active poll for team", function(done){
            axios.get(getActivePollsURL + "?TID=" + teamID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("activePolls");
                    
                    var activePolls = response.data.activePolls;
                    
                    expect(activePolls.constructor).to.equal(Array);
                    expect(activePolls.length).to.equal(1);
                    
                    var firstPollFound = false;
                    var secondPollFound = false;
                    
                    for(var i = 0; i < activePolls.length; i++){
                        var poll = activePolls[i];
                    
                        expect(poll.constructor).to.equal(Array);
                        expect(poll.length).to.equal(3);
                        
                        expect(poll[1]).to.equal(meetupID);
                        expect(poll[2]).to.equal(meetupName);
                        
                        if(poll[0] === pollID1){
                            firstPollFound = true;
                        }
                        
                        if(poll[0] === pollID2){
                            secondPollFound = true;
                        }
                    }
                    
                    expect(firstPollFound).to.equal(true);
                    expect(secondPollFound).to.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Doesn't allow user1 to vote on second poll", function(done){
            axios.put(voteURL + "?PID=" + pollID2, {
                "vote" : 0,
                "UID" : userID1
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns two votes for second poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID2)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions2);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions2[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(votes[0][1]).to.equal(1);
                    expect(votes[1][1]).to.equal(0);
                    expect(votes[2][1]).to.equal(1);
                    
                    expect(voteCounts).to.equal(2);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Doesn't allow user2 to change their mind on second poll", function(done){
            axios.put(voteURL + "?PID=" + pollID2, {
                "vote" : 1,
                "UID" : userID2
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(false);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Allows user2 to change their mind on first poll", function(done){
            axios.put(voteURL + "?PID=" + pollID1, {
                "vote" : 1,
                "UID" : userID2
                }).then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns three votes for first poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions1);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions1[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(votes[0][1]).to.equal(0);
                    expect(votes[1][1]).to.equal(3);
                    
                    expect(voteCounts).to.equal(3);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully closes first poll", function(done){
            axios.put(closePollURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Returns no active polls for team", function(done){
            axios.get(getActivePollsURL + "?TID=" + teamID)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("activePolls");
                    expect(response.data.activePolls.constructor).to.equal(Array);
                    expect(response.data.activePolls.length).to.equal(0);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns three votes for first poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions1);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions1[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(votes[0][1]).to.equal(0);
                    expect(votes[1][1]).to.equal(3);
                    
                    expect(voteCounts).to.equal(3);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Returns two votes for second poll", function(done){
            axios.get(getVotesURL + "?PID=" + pollID2)
                .then(function (response) {
                    expect(response).to.have.property("status");
                    expect(response.status).to.equal(200);
                    
                    expect(response).to.have.property("data");
                    expect(response.data).to.have.property("found");
                    expect(response.data.found).to.equal(true);
                    
                    expect(response.data).to.have.property("votes");
                    
                    var votes = response.data.votes;
                    
                    expect(votes.constructor).to.equal(Array);
                    expect(votes.length).to.equal(numOptions2);
                    
                    var voteCounts = 0;
                    
                    for(var i = 0; i < votes.length; i++){
                        var optionVotes = votes[i];
                        
                        expect(optionVotes.constructor).to.equal(Array);
                        expect(optionVotes.length).to.equal(2);
                        
                        expect(optionVotes[0]).to.equal(pollOptions2[i]);
                        
                        expect(typeof optionVotes[1]).to.equal("number");
                        voteCounts += optionVotes[1];
                    }
                    
                    expect(voteCounts).to.equal(2);
                    
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1); 
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully deletes first poll", function(done){
            axios.delete(deletePollURL + "?PID=" + pollID1)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
        it("Successfully deletes second poll", function(done){
            axios.delete(deletePollURL + "?PID=" + pollID2)
                .then(function (response) {
                    expect(response.status).to.equal(200);
                    expect(response.data).to.have.property("updated");
                    expect(response.data.updated).to.have.equal(true);
                }).catch(function (error) {
                    console.log(error); // should not be reached
                    expect(1).to.equal(-1);
                }).then(function () {
                    done();
            });
        });
        
    });
});