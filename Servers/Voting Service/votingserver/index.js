const express = require("express");
const path = require("path");
const app = express();
const firebase = require("firebase");
const axios = require("axios");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const admin = require("firebase-admin");
const fs = require('fs');
const utils = require("utils");
const im = require('istanbul-middleware');

//TODO: authentication - who is sending the get/post requests?

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

// Load service account secrets from a local file.
var db = {};
fs.readFile('/app/votingserver/firestoreadmin.json', (err, content) => {
    console.log(__dirname);
    
    if (err) return console.log('Error loading firestore admin file:', err);
  
    const serviceAccount = JSON.parse(content);
    
    // Initialize Firebase
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        apiKey: "AIzaSyDg8a7QE4sBTMaAJvELY6tqIP-dFdHmNmg",
        authDomain: "cpen321-voting.firebaseapp.com",
        databaseURL: "https://cpen321-voting.firebaseio.com",
        projectId: "cpen321-voting",
        storageBucket: "cpen321-voting.appspot.com",
        messagingSenderId: "971155439481"
    });
    
    // Get a reference to the firestore service
    db = admin.firestore();
});


/*==============================================
 * Assorted helper functions
 *==============================================*/
// function adds PID to array of active polls in the database
function addActivePoll(teamID, pollID, meetupID, meetupName){
    var docRef = db.collection("ActivePolls").doc(String(teamID));
    
    docRef.get().then(function(doc){
        if(!doc.exists){
            // Add teamID to database with given pollID
            docRef.set({
                activePolls:[pollID],
                meetupIDs: [meetupID],
                meetupNames: [meetupName]
            }).then(function(){
                    console.log("Successfully added TeamID " + teamID + " doc to ActivePolls");
                })
                .catch(function(error){
                    console.log("Unable to add TeamID " + teamID + " doc to ActivePolls: " + error);
                });
        } else{
            // add polls to already exisiting array of active polls
            var activePolls = doc.data().activePolls;
            activePolls.push(pollID);
            
            var meetupIDs = doc.data().meetupIDs;
            meetupIDs.push(meetupID);
            
            var meetupNames = doc.data().meetupNames;
            meetupNames.push(meetupName);
            
            docRef.update({
                activePolls,
                meetupIDs,
                meetupNames
                }).then(function() {
                    console.log("Successfully updated TeamID " + teamID + " doc in ActivePolls");
                })
                .catch(function(error) {
                    console.log("Failed to update TeamID " + teamID + " doc in ActivePolls");
                });
        }
    });
}

// function removes PID from the array of active polls in the database
function removeActivePoll(teamID, pollID){
    // see if teamID is already in database
    var docRef = db.collection("ActivePolls").doc(String(teamID));
        
    docRef.get().then(function(doc){
        if(!doc.exists){
            console.log("failed to locate teamID " + teamID + " in active polls.");
        } else{
            // find index of pollID
            var activePolls = doc.data().activePolls;
            
            var index = activePolls.indexOf(pollID);
            if(index > -1){
                var meetupIDs = doc.data().meetupIDs;
                var meetupNames = doc.data().meetupNames;
                
                activePolls.splice(index, 1);
                meetupIDs.splice(index, 1);
                meetupNames.splice(index, 1);
                
                docRef.update({
                    activePolls, 
                    meetupIDs,
                    meetupNames
                    }).then(function() {
                        console.log("Successfully updated TeamID " + teamID + " doc in ActivePolls");
                    })
                    .catch(function(error) {
                        console.log("Failed to update TeamID " + teamID + " doc in ActivePolls");
                    });
            }
            else{
                console.log("failed to locate pollID " + pollID + " in active polls.");
            }
            
        }
    });
}

// Server side code - allow console logging for automatic code review
/* eslint-disable no-console */

module.exports = {
    start: function(){
        app.set("port", (process.env.PORT || 5000));

        app.use(express.json());
        app.use(express.urlencoded());
        
        // for testing only - remove if deployed
        app.use('/coverage', im.createHandler()); 


        app.use(function(req,res,next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type,Accept");
            next();
        });

        // allows access from local host
        app.use("/", express.static(path.join(__dirname, "..")));
        
        
        app.listen(app.get("port"), function() {
          console.log("Node app is running at localhost:" + app.get("port"));
        });
                 
        /*==============================================
         * GET, POST, PUT requests
         *==============================================*/
         
        /*
         * POST endpoint which adds a new poll to the database, returning it's PID
         * 
         * Request requires:    "TID" - teamID of team that created the poll
         *                      "options" - options for users to vote on
         */
        app.post("/newPoll", function(myRequest, myResponse){
            
            console.log ("new poll");
            
            var options = myRequest.body.options;
            var teamID = myRequest.body.TID;
            var meetupID = myRequest.body.MID;
            var meetupName = myRequest.body.meetupName;
            
            console.log ("here");
            
            if(utils.checkIfUndefined([options, teamID, meetupID, meetupName])){
                var myErrorMessage = "400 Error: Expected new poll request to contain Team ID, MeetupID, meetupName and options for poll";
                utils.returnError(myResponse, myErrorMessage, 400);
            }
            else if(!utils.checkIfStrings([teamID, meetupID, meetupName])){
                var myErrorMessage = "400 Error: Expected new poll request to TID, MID, and meetupName as non emptystrings";
                utils.returnError(myResponse, myErrorMessage, 400);
            }            
            else if(options.constructor !== Array){
                var myErrorMessage = "400 Error: Expected options to be an array";
                utils.returnError(myResponse, myErrorMessage, 400);
            }            
            else{
                
                var optionCounts = [];
                for(var i = 0; i < options.length; i++){
                    optionCounts.push(0);
                }
                
                 console.log ("try to add new poll");
                
                // try to add new poll
                db.collection("Polls").add({
                    TID: teamID,
                    MID: meetupID,
                    meetupName,
                    optionNames: options,
                    optionCounts,
                    votes: {},
                    active: true,
                    startTime: admin.database.ServerValue.TIMESTAMP
                })
                .then(function(docRef){
                    // successfully added new poll
                    var pollID = docRef.id;
                    console.log("New poll created with PID = " + pollID);
                    
                    console.log("before addActivePoll");
                    addActivePoll(teamID, pollID, meetupID, meetupName);
                    
                    console.log("after addActivePoll");
                    myResponse.status = 200;
                    myResponse.json({"PID":pollID});
                    
                    myResponse.end();
                })
                .catch(function(error){
                     console.log ("error occured");
                    var myErrorMessage = "500 Error: Unable to add new poll, error returned from database: " + error;
                    utils.returnError(myResponse, myErrorMessage, 500);
                });
            }
        });

        /*
         * PUT endpoint which closes the poll with the id requested (PID)
         *
         * Expects a query string with the name "PID" and returns an error otherwise
         */
        app.put("/closePoll", function(myRequest, myResponse){
            
            var pollID = myRequest.query["PID"];
            
            if(typeof pollID !== "string" || pollID.length <= 0){
                var myErrorMessage = "400 Error: Expected PID in closePoll query as non-empty string";
                utils.returnError(myResponse, myErrorMessage, 400);
            } 
            else{
                
                var docRef = db.collection("Polls").doc(pollID);
                
                db.runTransaction(function(transaction){
                    return transaction.get(docRef).then(function(doc) {
                        if (!doc.exists) {
                            throw 400;
                        }
                        var active = doc.data().active;
                        var endTime = doc.data().endTime;
                        var teamID = doc.data().TID;
                        
                        if(active){
                            endTime = admin.database.ServerValue.TIMESTAMP;
                        }
                        
                        transaction.update(docRef, {
                            "active": false,
                            endTime
                        });
                        
                        removeActivePoll(teamID, pollID);
                        return active;
                    });
                    
                }).then(function(updated){
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                }).catch(function(error){
                    if(error === 400){
                        var myErrorMessage = "400 Error: Poll " + pollID + " is does not exist";
                        utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                    }
                    else{
                        var myErrorMessage = "500 Error: Unable to close poll, error returned: " + error;
                        utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                    }
                });
            }
        });

        /*
         * GET endpoint which attempts to fetch the votes belonging to the poll 
         * with the id requested (PID)
         * 
         * Expects a query string with the name "PID" and returns an error otherwise
         */
         app.get("/getVotes", function(myRequest, myResponse) {    
            
            var pollID = myRequest.query["PID"];
            
            if(typeof pollID !== "string" || pollID.length <= 0){
                var myErrorMessage = "400 Error: Expected PID in getVotes query as non empty string";
                utils.returnError(myResponse, myErrorMessage, 400);
            } 
            else{
                var docRef = db.collection("Polls").doc(String(pollID));
                
                docRef.get().then(function(doc){
                    myResponse.status = 200;
                    
                    if(!doc.exists){
                        console.log("PID " + pollID + " is invalid, returned empty");
                        
                        myResponse.json({"found":false});
                        
                        myResponse.end();
                    } else{
                        console.log("PID " + pollID + " is valid, returned votes");
                        
                        var pollJson = doc.data();
                        pollJson.found = true;
                        
                        var optionNames = doc.data().optionNames;
                        var optionCounts = doc.data().optionCounts;
                        
                        var returnVotes = [];
                        
                        for(var i = 0; i < optionNames.length; i++){
                            returnVotes.push([optionNames[i], optionCounts[i]]);
                        }
                        
                        myResponse.json({"found":true, "votes":returnVotes});
                        
                        myResponse.end();
                    }
                });
            }
            
        });

        /*
         * PUT endpoint which closes the poll with the id requested (PID)
         *
         * Expects a query string with the name "PID" and returns an error otherwise
         */
        app.put("/vote", function(myRequest, myResponse){
            
            var pollID = myRequest.query["PID"];
            
            var userVote = myRequest.body.vote;
            var userID = myRequest.body.UID;
            
            var myErrorMessage = "";
            
            if(utils.checkIfUndefined([pollID, userID, userVote])){
                myErrorMessage = "400 Error: Expected vote request to contain poll ID, user ID, and option number";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else if(!utils.checkIfStrings([pollID, userID])){
                myErrorMessage = "400 Error: Expected vote request to contain poll ID, and user ID as non empty strings";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else if (!Number.isInteger(userVote) || userVote < 0){
                myErrorMessage = "400 Error: user vote is supposed to be an integer >= 0";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var docRef = db.collection("Polls").doc(pollID);
                
                docRef.get().then(function(doc){
                    if(!doc.exists){
                        myErrorMessage = "400 Error: Poll " + pollID + " does not exist";
                        utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                    } else{
                        if(!doc.data().active){
                            myResponse.status = 200;
                            myResponse.json({"updated":false});
                            myResponse.end();
                        }
                        // ensure vote is valid
                        else if(userVote >= doc.data().optionNames.length){
                            myErrorMessage = "400 Error: user vote is invalid, out of bounds";
                            utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
                        }
                        else{
                            var votes = doc.data().votes;
                            var optionCounts = doc.data().optionCounts;

                            if(votes.hasOwnProperty(userID) ){
                                var oldVote = votes[userID];
                                optionCounts[oldVote] -= 1;
                            }
                            
                            votes[userID] = userVote;
                            optionCounts[userVote] += 1;
                            
                            docRef.update({
                                votes,
                                optionCounts
                                })
                                .then(function() {
                                    console.log("Poll with PID = " + pollID + " updated");
                                    
                                    myResponse.status = 200;
                                    
                                    myResponse.json({"updated":true});
                                    
                                    myResponse.end();
                                })
                                .catch(function(error) {
                                    myErrorMessage = "500 Error: Unable to vote on poll, error returned from database: " + error;
                                    utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                                });
                        }
                    }
                });
            }
        });


        /*
         * GET endpoint which attempts to fetch the active polls of a given team
         * 
         * Expects a query string with the name "TID" and returns an error otherwise
         */
         app.get("/getActivePolls", function(myRequest, myResponse) {    
            
            var teamID = myRequest.query["TID"];
            
            if(typeof teamID !== "string" || teamID.length <= 0){
                var myErrorMessage = "400 Error: Expected TID in getActivePolls query";
                utils.returnError(myResponse, myErrorMessage, 400);
            } 
            else{
                var docRef = db.collection("ActivePolls").doc(teamID);
                
                docRef.get().then(function(doc){
                    myResponse.status = 200;
                    
                    if(!doc.exists){
                        console.log("TID " + teamID + " has no activePolls");
                        
                        myResponse.json({"activePolls":[]});
                        
                        myResponse.end();
                    } else{
                        console.log("Returned active polls belonging to TID " + teamID);
                        
                        var activePolls = doc.data().activePolls;
                        var meetupIDs = doc.data().meetupIDs;
                        var meetupNames = doc.data().meetupNames;
                        
                        var combinedArray = [];
                        
                        while(activePolls.length > 0){
                            combinedArray.push([activePolls.pop(), meetupIDs.pop(), meetupNames.pop()]);
                        }
                        
                        myResponse.json({"activePolls":combinedArray});
                        
                        myResponse.end();
                    }
                });
            }
        });

        /*
         * DELETE request endpoint which removes the poll
         *
         * query requirements:  - PID : string
         * body requirements:   None
         *
         * returned JSON:       - updated : bool; true if poll was present in database and was deleted
         *                                  if PID is not found in database, they are not updated.          
         *
         * returns error if PID is not present in query, updated false on error
         */
        app.delete("/deletePoll", function(myRequest, myResponse) {
            var pollID = myRequest.query.PID;
            
            var myErrorMessage = "";
            
            if(typeof pollID !== "string" || pollID.length <= 0){
                myErrorMessage = "400 Error: Expected PID in deletePoll as a string";
                utils.returnError(myResponse, myErrorMessage, 400, {"updated":false});
            }
            else{
                var docRef = db.collection("Polls").doc(pollID);
                
                return db.runTransaction(function(transaction) {
                    return transaction.get(docRef).then(function(doc) {
                        if (!doc.exists) {
                            return false;
                        }
                        
                        transaction.delete(docRef);
                        return true;
                    });
                }).then(function(updated) {
                    myResponse.status = 200;
                    myResponse.json({updated});
                    myResponse.end();
                }).catch(function(error) {
                    myErrorMessage = "500 Error: Unable to delete user, error returned: " + error;
                    utils.returnError(myResponse, myErrorMessage, 500, {"updated":false});
                });
            }
            
        });

    }
};