package com.example.cindy.bitswhere;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class JoinTeamEspressoTest {

    private String teamID;

    @Rule
    public ActivityTestRule<HomeScreenActivity> mActivityRule
            = new ActivityTestRule<HomeScreenActivity>(HomeScreenActivity.class){
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, HomeScreenActivity.class);
            result.putExtra("UID","108296247272471916735");
            return result;
        }
    };

    @Before
    public void initValidString() {
        // Specify a valid string.
        teamID = "1mtzErmJWBTscqFbMuNX";
    }

    @Test
    public void checkDialogDisplayed() {

        // Click on the button that shows the dialog
        onView(withId(R.id.btnJoinTeam)).perform(ViewActions.click());

        // Check the dialog title text is displayed
        onView(withText(R.string.join_team_dialog_heading)).check(matches(withText("Join a Team")));

        // Check the dialog input hint is displayed
        onView(withId(R.id.input)).check(matches(withHint("enter Team ID...")));

        //check the cancel button can properly close the dialog
        onView(withId(R.id.action_cancel)).perform(ViewActions.click()).check(ViewAssertions.doesNotExist());

    }

    @Test
    public void checkTeamJoined() {

        // Click on the button that shows the dialog
        onView(withId(R.id.btnJoinTeam)).perform(ViewActions.click());

//       // Type the teamID into the dialog input
        onView(withId(R.id.input)).perform(ViewActions.typeText(teamID));

        //check the ok button can properly close the dialog
        onView(withId(R.id.action_ok)).perform(ViewActions.click()).check(ViewAssertions.doesNotExist());

        //check Toast Visibility
        onView(withText("Success joining the team!")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));

    }


}
