package com.example.cindy.bitswhere;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.design.widget.CoordinatorLayout;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class ViewActivePollEspressoTest {private String teamName;

    @Rule
    public ActivityTestRule<ViewPollActivity> mActivityRule
            = new ActivityTestRule<ViewPollActivity>(ViewPollActivity.class){
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, ViewPollActivity.class);
            result.putExtra("UID","114918733428212357677");
            result.putExtra("TID","rCies8KSr3tjm2Cgyd1E");
            return result;
        }
    };

    @Before
    public void initValidString() {
        // Specify a valid string.
        teamName = "CPSC314";
    }


    @Test
    public void checkExpandableListDisplayed(){
        //Click on view team button
//        onView(withId(R.id.btnViewTeam)).perform(ViewActions.click());
        SystemClock.sleep(1000);

        //Check the List is collapse
        onView(ViewMatchers.withId(R.id.lblListHeader)).perform(ViewActions.click());
        onView(ViewMatchers.withId(R.id.lblListItem)).check(ViewAssertions.doesNotExist());
        //Click the List header
        onView(ViewMatchers.withId(R.id.lblListHeader)).perform(ViewActions.click());
        onView(withIndex(withId(R.id.lblListItem), 0)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(withIndex(withId(R.id.lblListItem), 0)).perform(ViewActions.click());

//        onView(withIndex(withId(R.id.lblListItem), 0)).perform(ViewActions.click());
//        onView(withIndex(withId(R.id.lblListItem), 0)).check(matches(withText(teamName)));

    }

    @Test
    public void checkVote(){
        //Click on view team button
//        onView(withId(R.id.btnViewTeam)).perform(ViewActions.click());
        SystemClock.sleep(1000);

        onView(withIndex(withId(R.id.lblListItem), 0)).perform(ViewActions.click());
        onView(ViewMatchers.withId(R.id.lblListItem)).check(ViewAssertions.doesNotExist());
        onView(withClassName(Matchers.equalTo(CoordinatorLayout.class.getName()))).check(matches(withId(R.id.voteActivity)));
        onView(withId(R.id.backtomain)).perform(ViewActions.click());
        onView(withClassName(Matchers.equalTo(CoordinatorLayout.class.getName()))).check(matches(withId(R.id.viewPollActivity)));

//        onView(withIndex(withId(R.id.lblListItem), 0)).perform(ViewActions.click());
//        onView(withIndex(withId(R.id.lblListItem), 0)).check(matches(withText(teamName)));

    }

    @Test
    public void checkLogOut(){
        onView(withId(R.id.logout)).perform(ViewActions.click());

        //Check clicking view team button launch main activity
        onView(withId(R.id.logout)).check(ViewAssertions.doesNotExist());
        onView(withClassName(Matchers.equalTo(CoordinatorLayout.class.getName()))).check(matches(withId(R.id.mainActivity)));
    }



    public static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }



}
