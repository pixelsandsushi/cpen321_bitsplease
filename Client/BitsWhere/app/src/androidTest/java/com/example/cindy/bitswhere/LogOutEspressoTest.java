package com.example.cindy.bitswhere;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.design.widget.CoordinatorLayout;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LogOutEspressoTest {
    private String teamID;

    @Rule
    public ActivityTestRule<HomeScreenActivity> mActivityRule
            = new ActivityTestRule<HomeScreenActivity>(HomeScreenActivity.class){
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, HomeScreenActivity.class);
            result.putExtra("UID","108296247272471916735");
            return result;
        }
    };


    @Test
    public void checkLogOut() {

        // Click on the button that shows the dialog
        onView(withId(R.id.logout)).perform(ViewActions.click());

        //Check clicking view team button launch the view team activity
        onView(withId(R.id.logout)).check(ViewAssertions.doesNotExist());
        onView(withClassName(Matchers.equalTo(CoordinatorLayout.class.getName()))).check(matches(withId(R.id.mainActivity)));
        onView(withId(R.id.fab)).check(matches(isDisplayed()));
        onView(withId(R.id.sign_out_button)).check(matches(isDisplayed()));
        onView(withId(R.id.sign_out_and_disconnect)).check(matches(isDisplayed()));
        onView(withId(R.id.sign_out_button)).perform(ViewActions.click());
        SystemClock.sleep(1000);
        onView(withId(R.id.fab)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.sign_out_and_disconnect)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.sign_out_button)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.sign_in_button)).perform(ViewActions.click());
        SystemClock.sleep(1000);
        onView(withId(R.id.fab)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(withId(R.id.sign_out_and_disconnect)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(withId(R.id.sign_out_button)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        onView(withId(R.id.fab)).perform(ViewActions.click());
        onView(withId(R.id.homeScreenActivity)).check(matches(isDisplayed()));

    }

    public static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }

}

