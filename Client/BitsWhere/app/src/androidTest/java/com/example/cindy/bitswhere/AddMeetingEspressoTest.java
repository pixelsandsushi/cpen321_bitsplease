package com.example.cindy.bitswhere;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.design.widget.CoordinatorLayout;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.DatePicker;
import android.widget.TimePicker;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;


@RunWith(AndroidJUnit4.class)
public class AddMeetingEspressoTest {

    private String title;

    @Rule
    public ActivityTestRule<AddPollActivity> mActivityRule
            = new ActivityTestRule<AddPollActivity>(AddPollActivity.class){
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, AddPollActivity.class);
            result.putExtra("UID","114918733428212357677");
            result.putExtra("TID","rCies8KSr3tjm2Cgyd1E");
            return result;
        }
    };

    @Before
    public void initValidString() {
        // Specify a valid string.
        title = "meet up 1";
    }


    @Test
    public void testVisibility(){
        onView(withId(R.id.tvStartDate)).check(matches(withText("Select Start Date")));
        onView(withId(R.id.tvStartTime)).check(matches(withText("Select Start Time")));
        onView(withId(R.id.tvEndDate)).check(matches(withText("Select End Date")));
        onView(withId(R.id.tvEndTime)).check(matches(withText("Select End Time")));
        onView(withId(R.id.btnCreate)).check(matches(withText("Create")));
    }

//    @Test
//    public void testTitle(){
//
//        // Check the dialog input hint is displayed
//        onView(withId(R.id.editTitle)).check(matches(withHint("Title")));
//        onView(withId(R.id.editTitle)).perform(ViewActions.typeText(title));
//        onView(withId(R.id.editTitle)).check(matches(withText(title)));
//
//
//    }
    @Test
    public void testCreateMeetUP(){
        // Check the dialog input hint is displayed
        onView(withId(R.id.editTitle)).check(matches(withHint("Title")));
        onView(withId(R.id.editTitle)).perform(ViewActions.typeText(title));
        onView(withId(R.id.editTitle)).check(matches(withText(title)));

        //startDate
        onView(withId(R.id.tvStartDate)).check(matches(withText("Select Start Date")));
        onView(withId(R.id.tvStartDate)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2018, 11, 20));
        SystemClock.sleep(2000);
//        onView(withText("Done")).perform(ViewActions.click());
//        SystemClock.sleep(1000);

        //startTime
        onView(withId(R.id.tvStartTime)).check(matches(withText("Select Start Time")));
        onView(withId(R.id.tvStartTime)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(13, 50));
        SystemClock.sleep(2000);
//        onView(allOf(withClassName(endsWith("AppCompatButton")), withText(is("Done")))).perform(ViewActions.click());
//        SystemClock.sleep(1000);
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName())))
                .check(ViewAssertions.doesNotExist());

        //endDate
        onView(withId(R.id.tvEndDate)).check(matches(withText("Select End Date")));
        onView(withId(R.id.tvEndDate)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2018, 11, 20));
        SystemClock.sleep(2000);
//        onView(withText("Done")).perform(ViewActions.click());
//        SystemClock.sleep(1000);
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
                .check(ViewAssertions.doesNotExist());

        //endTime
        onView(withId(R.id.tvEndTime)).check(matches(withText("Select End Time")));
        onView(withId(R.id.tvEndTime)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(15, 50));
        SystemClock.sleep(2000);
//        onView(withText("Done")).perform(ViewActions.click());
//        onView(allOf(withClassName(endsWith("AppCompatButton")), withText(is("Done")))).perform(ViewActions.click());
//        SystemClock.sleep(1000);
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName())))
                .check(ViewAssertions.doesNotExist());

        onView(withId(R.id.btnCreate)).perform(click());
        SystemClock.sleep(3000);
        onView(withId(R.id.btnCreate)).check(ViewAssertions.doesNotExist());


    }

    @Test
    public void checkLogOut(){
        onView(withId(R.id.logout)).perform(ViewActions.click());

        //Check clicking view team button launch the main activity
        onView(withId(R.id.logout)).check(ViewAssertions.doesNotExist());
        onView(withClassName(Matchers.equalTo(CoordinatorLayout.class.getName()))).check(matches(withId(R.id.mainActivity)));

    }

//    @Test
//    public void testStartDate(){
//        onView(withId(R.id.tvStartDate)).check(matches(withText("Select Start Date")));
//        onView(withId(R.id.tvStartDate)).perform(click());
//        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2018, 11, 20));
//        SystemClock.sleep(1000);
//        onView(withText("Done")).perform(ViewActions.click());
//        SystemClock.sleep(1000);
//        onView(withId(R.id.tvStartTime)).perform(click());
//
//    }
//
//    @Test
//    public void testEndDate(){
//        onView(withId(R.id.tvEndDate)).check(matches(withText("Select End Date")));
//        onView(withId(R.id.tvEndDate)).perform(click());
//        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2018, 11, 20));
//        SystemClock.sleep(1000);
//        onView(withText("Done")).perform(ViewActions.click());
//        SystemClock.sleep(1000);
//        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
//                .check(ViewAssertions.doesNotExist());
//    }
//    @Test
//    public void testStartTime(){
//        onView(withId(R.id.tvStartTime)).check(matches(withText("Select Start Time")));
//        onView(withId(R.id.tvStartTime)).perform(click());
//        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(13, 50));
//        SystemClock.sleep(1000);
////        onView(withText("Done")).perform(ViewActions.click());
//        onView(allOf(withClassName(endsWith("AppCompatButton")), withText(is("Done")))).perform(ViewActions.click());
//        SystemClock.sleep(1000);
//        onView(withClassName(Matchers.equalTo(TimePicker.class.getName())))
//                .check(ViewAssertions.doesNotExist());
//    }
//    @Test
//    public void testEndTime(){
//        onView(withId(R.id.tvEndTime)).check(matches(withText("Select End Time")));
//        onView(withId(R.id.tvEndTime)).perform(click());
//        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(14, 50));
//        SystemClock.sleep(1000);
////        onView(withText("Done")).perform(ViewActions.click());
//        onView(allOf(withClassName(endsWith("AppCompatButton")), withText(is("Done")))).perform(ViewActions.click());
//        SystemClock.sleep(1000);
//        onView(withClassName(Matchers.equalTo(TimePicker.class.getName())))
//                .check(ViewAssertions.doesNotExist());
//    }


}
