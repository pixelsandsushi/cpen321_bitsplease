package com.example.cindy.bitswhere;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.design.widget.CoordinatorLayout;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class VoteEspressoTest {

    @Rule
    public ActivityTestRule<VoteActivity> mActivityRule
            = new ActivityTestRule<VoteActivity>(VoteActivity.class){
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, VoteActivity.class);
            result.putExtra("UID","114918733428212357677");
            result.putExtra("TID","rCies8KSr3tjm2Cgyd1E");
            result.putExtra("PID","CPTfYq8C50VvLAETj86f");
            return result;
        }
    };

    @Test
    public void checkDialogDisplayed() {

        SystemClock.sleep(1000);
        // Click on the button that shows the dialog
        onView(withId(R.id.viewResult)).perform(ViewActions.click());
        SystemClock.sleep(1000);

        // Check the dialog title text is displayed
        onView(withText(R.string.view_result_dialog_heading)).check(matches(withText("Vote Result")));

        //check the cancel button can properly close the dialog
        onView(withId(R.id.action_ok)).perform(ViewActions.click()).check(ViewAssertions.doesNotExist());

    }

    @Test
    public void checkErrorChecking() {
        SystemClock.sleep(1000);
        onView(withId(R.id.btnVote)).perform(ViewActions.click());
        onView(withText("Please select an option")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(1000);
    }




    @Test
    public void checkPollDisplayed() {

        SystemClock.sleep(1000);
        // Click on the button that shows the dialog
        onView(withId(R.id.radioGroupPoll)).check(matches(isDisplayed()));

        onView(withId(R.id.radionbtnA)).check(matches(isDisplayed()));
        onView(withId(R.id.radionbtnB)).check(matches(isDisplayed()));
        onView(withId(R.id.radionbtnC)).check(matches(isDisplayed()));
        onView(withId(R.id.radionbtnD)).check(matches(isDisplayed()));

    }

    @Test
    public void checkOptionA() {

        SystemClock.sleep(1000);
        // Click on the button that shows the dialog
        onView(withId(R.id.radioGroupPoll)).check(matches(isDisplayed()));

        onView(withId(R.id.radionbtnA)).perform(ViewActions.click());
        onView(withText("A selected")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(2000);

        onView(withId(R.id.btnVote)).perform(ViewActions.click());
        onView(withText("I voted for: A")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(1000);

    }

    @Test
    public void checkOptionB() {

        SystemClock.sleep(1000);
        // Click on the button that shows the dialog
        onView(withId(R.id.radioGroupPoll)).check(matches(isDisplayed()));

        onView(withId(R.id.radionbtnB)).perform(ViewActions.click());
        onView(withText("B selected")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(2000);

        onView(withId(R.id.btnVote)).perform(ViewActions.click());
        onView(withText("I voted for: B")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(1000);


    }

    @Test
    public void checkOptionC() {

        SystemClock.sleep(1000);
        // Click on the button that shows the dialog
        onView(withId(R.id.radioGroupPoll)).check(matches(isDisplayed()));

        onView(withId(R.id.radionbtnC)).perform(ViewActions.click());
        onView(withText("C selected")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(2000);

        onView(withId(R.id.btnVote)).perform(ViewActions.click());
        onView(withText("I voted for: C")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(1000);

    }

    @Test
    public void checkOptionD() {

        SystemClock.sleep(1000);
        // Click on the button that shows the dialog
        onView(withId(R.id.radioGroupPoll)).check(matches(isDisplayed()));

        onView(withId(R.id.radionbtnD)).perform(ViewActions.click());
        onView(withText("D selected")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(2000);

        onView(withId(R.id.btnVote)).perform(ViewActions.click());
        onView(withText("I voted for: D")).
                inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
        SystemClock.sleep(1000);

    }


    @Test
    public void checkLogOut(){
        onView(withId(R.id.logout)).perform(ViewActions.click());

        //Check clicking view team button launch the main activity
        onView(withId(R.id.logout)).check(ViewAssertions.doesNotExist());
        onView(withClassName(Matchers.equalTo(CoordinatorLayout.class.getName()))).check(matches(withId(R.id.mainActivity)));

    }






}

