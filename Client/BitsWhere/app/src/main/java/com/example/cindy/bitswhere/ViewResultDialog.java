package com.example.cindy.bitswhere;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;

public class ViewResultDialog extends DialogFragment {

    private static final String TAG = "ViewResultDialog";
    private TextView mActionOk;
    private HorizontalBarChart chart;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        VoteActivity activity = (VoteActivity) getActivity();
        ArrayList<Integer> result = getArguments().getIntegerArrayList("voteNumber");
        ArrayList<BarEntry> values = new ArrayList<>();

        View view = inflater.inflate(R.layout.dialog_view_result, container, false);
//        mText = view.findViewById(R.id.voteNumberText);
//        mText.setText(result.get(0)+" "+result.get(1)+" "+result.get(2)+" "+result.get(3));
//        mActionCancel = view.findViewById(R.id.action_cancel);
        mActionOk = view.findViewById(R.id.action_ok);
        chart = view.findViewById(R.id.chart1);


        ArrayList<BarEntry> entries = new ArrayList();
        ArrayList<String> XLabel = new ArrayList<>();
        XLabel.add("A");
        XLabel.add("B");
        XLabel.add("C");
        XLabel.add("D");

        entries.add(new BarEntry(0f, result.get(3)));
        entries.add(new BarEntry(1f, result.get(2)));
        entries.add(new BarEntry(2f, result.get(1)));
        entries.add(new BarEntry(3f, result.get(0)));

        BarDataSet dataset = new BarDataSet(entries,"vote result");
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        BarData data = new BarData(dataset);
        data.setValueFormatter(new MyValueFormatter());
        chart.setData(data);
        chart.invalidate();
        chart.setDrawBarShadow(false);
        chart.setPinchZoom(false);
        chart.setDrawValueAboveBar(false);
        chart.setDrawGridBackground(false);

        String[] option = new String[]{"D","C","B","A"};
        chart.setClickable(false);
        chart.getLegend().setEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setPinchZoom(false);

        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setDrawAxisLine(false);
        chart.getXAxis().disableGridDashedLine();
        chart.getXAxis().setEnabled(true);
        chart.getXAxis().setLabelCount(4);
        chart.getXAxis().setValueFormatter(new MyXAxisValueFormatter(option));
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        chart.getAxisRight().disableGridDashedLine();
        chart.getAxisRight().setDrawTopYLabelEntry(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getAxisRight().setDrawAxisLine(false);

        chart.getAxisLeft().disableGridDashedLine();
        chart.getAxisLeft().setDrawTopYLabelEntry(false);
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisLeft().setDrawAxisLine(false);



//        mActionCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick: closing dialog");
//                getDialog().dismiss();
//            }
//        });


        mActionOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing dialog");
                getDialog().dismiss();
            }
        });

        return view;
    }

    public class MyValueFormatter implements IValueFormatter {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return Math.round(value)+"";
        }
    }
    public class MyXAxisValueFormatter implements IAxisValueFormatter {

        public String[] values;

        public MyXAxisValueFormatter(String[] values) {
            this.values = values;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return this.values[(int) value];

        }
    }



}
