package com.example.cindy.bitswhere;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewPollActivity extends AppCompatActivity {

    public String teamID,userID;
    private JSONArray pollAttributes = new JSONArray();
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private List<String> list1 = new ArrayList<String>();
    private String pollID;
    private ImageView logOut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpoll);
        Bundle extras = getIntent().getExtras();
        userID = extras.getString("UID");
        teamID = extras.getString("TID");

        getPollRequest();

        FloatingActionButton fab = findViewById(R.id.addMeetup);
        FloatingActionButton backtoViewTeam = findViewById(R.id.backToViewTeam);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ViewPollActivity.this,
                        AddPollActivity.class);

                myIntent.putExtra("UID", userID);
                myIntent.putExtra("TID", teamID);

                startActivity(myIntent);
            }
        });

        backtoViewTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ViewPollActivity.this,
                        ViewTeamActivity.class);
                myIntent.putExtra("UID", userID);
                myIntent.putExtra("TID", teamID);
                startActivity(myIntent);
            }
        });

        logOut = findViewById(R.id.logout);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(ViewPollActivity.this,
                        MainActivity.class);

                myIntent.putExtra("UID",userID);
                startActivity(myIntent);

            }
        });
    }

    private void getPollRequest(){

        RequestQueue queue = Volley.newRequestQueue(ViewPollActivity.this);
        String url = "https://bitsplease-voting-service.herokuapp.com/getActivePolls/?TID=" + teamID;
        JSONObject json = new JSONObject();

        // Request a string response from the provided URL.
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            pollAttributes = response.getJSONArray("activePolls");
                            // get the listview
                            expListView = (ExpandableListView) findViewById(R.id.expPoll2);
                            // preparing list data
                            listDataHeader = new ArrayList<String>();
                            listDataChild = new HashMap<String, List<String>>();

                            // Adding child data
                            listDataHeader.add("Active Polls");

                            // Adding child data
                            for (int i = 0; i < pollAttributes.length(); i++) {
                                list1.add(pollAttributes.getJSONArray(i).getString(2));
                            }

                            listDataChild.put(listDataHeader.get(0), list1); // Header, Child data
                            listAdapter = new ExpandableListAdapter(ViewPollActivity.this, listDataHeader, listDataChild);

                            // setting list adapter
                            expListView.setAdapter(listAdapter);
                            expListView.expandGroup(0);

                            expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                @Override
                                public boolean onChildClick(ExpandableListView expandableListView, View view,
                                                            int groupPosition, int childPosition, long l) {
                                    try{
                                        pollID = pollAttributes.getJSONArray(childPosition).getString(0);

                                    }catch (JSONException e) {
                                        Toast.makeText(ViewPollActivity.this, "Error Getting JSON",Toast.LENGTH_SHORT).show();
                                    }

                                    Toast.makeText(ViewPollActivity.this, list1.get(childPosition),Toast.LENGTH_SHORT).show();
                                    Intent myIntent = new Intent(ViewPollActivity.this,
                                            VoteActivity.class);
                                    myIntent.putExtra("PID",pollID);
                                    myIntent.putExtra("TID",teamID);
                                    myIntent.putExtra("UID",userID);
                                    startActivity(myIntent);

                                    return false;
                                }
                            });

                        } catch (JSONException e) {
                            Toast.makeText(ViewPollActivity.this, "Error Getting JSON",Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ViewPollActivity.this, "This didn't work",Toast.LENGTH_SHORT).show();

            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);

    }


}
