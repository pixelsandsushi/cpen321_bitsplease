package com.example.cindy.bitswhere;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewTeamActivity extends AppCompatActivity {

    public JSONArray teamAttributes = new JSONArray();
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private List<String> list1 = new ArrayList<String>();
    public String userID;
    private String TID;
    private ImageView logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_team);
        Bundle extras = getIntent().getExtras();
        userID = extras.getString("UID");
        getTeamRequest();
        FloatingActionButton backtoHomePage = findViewById(R.id.backToHome);
        backtoHomePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ViewTeamActivity.this,
                        HomeScreenActivity.class);
                myIntent.putExtra("UID", userID);
                startActivity(myIntent);
            }
        });

        logOut = findViewById(R.id.logout);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(ViewTeamActivity.this,
                        MainActivity.class);

                myIntent.putExtra("UID",userID);
                startActivity(myIntent);

            }
        });
    }

    private void getTeamRequest(){

        EspressoIdlingResource.increment();

        RequestQueue queue = Volley.newRequestQueue(ViewTeamActivity.this);
        String url = "https://bitsplease-user-service.herokuapp.com/getTeams/?UID=" + userID;
        JSONObject json = new JSONObject();

        // Request a string response from the provided URL.
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            //teams[TID,TeamName]

                            teamAttributes = response.getJSONArray("teams");


                            // get the listview
                            expListView = (ExpandableListView) findViewById(R.id.expPoll);
                            // preparing list data
                            listDataHeader = new ArrayList<String>();
                            listDataChild = new HashMap<String, List<String>>();

                            // Adding child data
                            listDataHeader.add("Your Teams");

                            // Adding child data
                            for (int i = 0; i < teamAttributes.length(); i++) {
                                list1.add(teamAttributes.getJSONArray(i).getString(1));

                            }

                            listDataChild.put(listDataHeader.get(0), list1); // Header, Child data
                            listAdapter = new ExpandableListAdapter(ViewTeamActivity.this, listDataHeader, listDataChild);

                            // setting list adapter
                            expListView.setAdapter(listAdapter);
                            expListView.expandGroup(0);

                            expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                @Override
                                public boolean onChildClick(ExpandableListView expandableListView, View view,
                                                            int groupPosition, int childPosition, long l) {
                                    try{
                                        TID = teamAttributes.getJSONArray(childPosition).getString(0);

                                    }catch (JSONException e) {
                                        Toast.makeText(ViewTeamActivity.this, "Error Getting JSON",Toast.LENGTH_SHORT).show();
                                    }

                                    Toast.makeText(ViewTeamActivity.this, list1.get(childPosition),Toast.LENGTH_SHORT).show();
                                    Intent myIntent = new Intent(ViewTeamActivity.this,
                                            ViewPollActivity.class);
                                    myIntent.putExtra("TID",TID);
                                    myIntent.putExtra("UID",userID);
                                    startActivity(myIntent);

                                    return false;
                                }
                            });

                        } catch (JSONException e) {
                            Toast.makeText(ViewTeamActivity.this, "Error Getting JSON",Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ViewTeamActivity.this, "This didn't work",Toast.LENGTH_SHORT).show();

            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);

        if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
            EspressoIdlingResource.decrement(); // Set app as idle.
        }

    }
}
