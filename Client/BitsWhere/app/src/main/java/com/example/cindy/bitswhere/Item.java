package com.example.cindy.bitswhere;

/**
 * Created by Cindy on 2018-10-27.
 */


public class Item {

    private String name;
    private boolean checkbox;

    public Item() {
         /*Empty Constructor*/
    }
    public  Item(String location, boolean status){
        this.name = location;
        this.checkbox = status;
    }
    //Getter and Setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }

}