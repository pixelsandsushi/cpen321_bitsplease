package com.example.cindy.bitswhere;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class VoteActivity extends AppCompatActivity {

    private String isFound;

    private JSONArray voteAttributes;
    private Integer voteIndex;
    private String voteResultToast;
    public String voteNumber;


    private Button buttonVote,buttonResult;
    private RadioButton optionA, optionB, optionC, optionD;
    public Integer numberA = 0, numberB = 0, numberC = 0, numberD = 0;
    private TextView mTextView;
    private TextView pidTextView;
    private String teamID, userID;

    private String pidValue;
    private ImageView logOut;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Bundle extras = getIntent().getExtras();
        pidValue = extras.getString("PID");
        userID = extras.getString("UID");
        teamID = extras.getString("TID");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);

        if (pidValue !=null) {
            pidTextView =  findViewById(R.id.meetUpText);
//            pidTextView.setText(pidValue);
            getVoteRequest();
        }

        mTextView = (TextView) findViewById(R.id.meetUpText);

        buttonVote = (Button) findViewById(R.id.btnVote);
        buttonVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                putVoteRequest();
                getVoteRequest();

            }
        });

        buttonResult = findViewById(R.id.viewResult);
        buttonResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getVoteNumberRequest();

            }
        });

        FloatingActionButton backtoMain = findViewById(R.id.backtomain);
        backtoMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(VoteActivity.this,
                        ViewPollActivity.class);
                myIntent.putExtra("TID",teamID);
                myIntent.putExtra("UID",userID);
                startActivity(myIntent);

            }
        });

        logOut = findViewById(R.id.logout);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(VoteActivity.this,
                        MainActivity.class);

                myIntent.putExtra("UID",userID);
                startActivity(myIntent);

            }
        });
    }

    public void votePoll(View view){
        boolean checked = ((RadioButton) view).isChecked();
        String str = "";
        switch (view.getId()){
            case R.id.radionbtnA:
                if(checked)
                    str = "A selected";
                break;
            case R.id.radionbtnB:
                if(checked)
                    str = "B selected";
                break;

            case R.id.radionbtnC:
                if(checked)
                    str = "C selected";
                break;
            case R.id.radionbtnD:
                if(checked)
                    str = "D selected";
                break;

        }
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();

    }


    private void getVoteRequest(){

        RequestQueue queue = Volley.newRequestQueue(VoteActivity.this);
        String url = "https://bitsplease-voting-service.herokuapp.com/getVotes/?PID=" + pidValue;
        JSONObject json = new JSONObject();

        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            isFound = response.getString("found");
                            voteAttributes = response.getJSONArray("votes");

                            optionA = (RadioButton) findViewById(R.id.radionbtnA) ;
                            optionA.setText(voteAttributes.getJSONArray(0).getString(0));
                            optionB = (RadioButton) findViewById(R.id.radionbtnB) ;
                            optionB.setText(voteAttributes.getJSONArray(1).getString(0));
                            optionC = (RadioButton) findViewById(R.id.radionbtnC) ;
                            optionC.setText(voteAttributes.getJSONArray(2).getString(0));
                            optionD = (RadioButton) findViewById(R.id.radionbtnD) ;
                            optionD.setText(voteAttributes.getJSONArray(3).getString(0));

//                            voteNumber = voteAttributes.getJSONArray(0).getString(1) +" "+
//                                    voteAttributes.getJSONArray(1).getString(1) +" "+
//                                    voteAttributes.getJSONArray(2).getString(1) +" "+
//                                    voteAttributes.getJSONArray(3).getString(1);

                            numberA = Integer.parseInt(voteAttributes.getJSONArray(0).getString(1));
                            numberB = Integer.parseInt(voteAttributes.getJSONArray(1).getString(1));
                            numberC = Integer.parseInt(voteAttributes.getJSONArray(2).getString(1));
                            numberD = Integer.parseInt(voteAttributes.getJSONArray(3).getString(1));

                        } catch (JSONException e) {
                            Toast.makeText(VoteActivity.this, "Error Getting JSON",Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(VoteActivity.this, "This didn't work",Toast.LENGTH_SHORT).show();

            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);

    }

    private void putVoteRequest(){
        voteIndex = 0;
        voteIndex+= (optionA.isChecked())? 0:(optionB.isChecked())?1:(optionC.isChecked())?2:(optionD.isChecked())?3:0;
        voteResultToast = "I voted for: ";
        voteResultToast+= (optionA.isChecked())? "A":(optionB.isChecked())?"B":(optionC.isChecked())?"C":(optionD.isChecked())?"D":"";
        if(voteResultToast.equals("I voted for: "))
            voteResultToast = "Please select an option";
        JSONObject voteResult = new JSONObject();


        try {
            voteResult.put("vote",voteIndex);
            voteResult.put("UID","cindy");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestQueue voteResultQueue = Volley.newRequestQueue(VoteActivity.this);
        String putVoteURL = "https://bitsplease-voting-service.herokuapp.com/vote/?PID=" + pidValue;
        JsonObjectRequest voteRequestObject = new JsonObjectRequest(Request.Method.PUT, putVoteURL, voteResult,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Toast.makeText(VoteActivity.this, voteResultToast,Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(VoteActivity.this, "THAT DIDN'T WORK",Toast.LENGTH_SHORT).show();
            }
        });
        voteResultQueue.add(voteRequestObject);
    }

    private void getVoteNumberRequest(){

        RequestQueue queue = Volley.newRequestQueue(VoteActivity.this);
        String url = "https://bitsplease-voting-service.herokuapp.com/getVotes/?PID=" + pidValue;
        JSONObject json = new JSONObject();

        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            isFound = response.getString("found");
                            voteAttributes = response.getJSONArray("votes");
                            numberA = Integer.parseInt(voteAttributes.getJSONArray(0).getString(1));
                            numberB = Integer.parseInt(voteAttributes.getJSONArray(1).getString(1));
                            numberC = Integer.parseInt(voteAttributes.getJSONArray(2).getString(1));
                            numberD = Integer.parseInt(voteAttributes.getJSONArray(3).getString(1));
                            ArrayList<Integer> result = new ArrayList<Integer>();
                            result.add(numberA);
                            result.add(numberB);
                            result.add(numberC);
                            result.add(numberD);
                            Bundle data = new Bundle();//create bundle instance
                            data.putIntegerArrayList("voteNumber",result);

                            FragmentManager fm = getSupportFragmentManager();
                            ViewResultDialog dialog = new ViewResultDialog();
                            dialog.setArguments(data);
                            dialog.show(fm,"ViewResultDialog");

                        } catch (JSONException e) {
                            Toast.makeText(VoteActivity.this, "Error Getting JSON",Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(VoteActivity.this, "This didn't work",Toast.LENGTH_SHORT).show();

            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);

    }


}
