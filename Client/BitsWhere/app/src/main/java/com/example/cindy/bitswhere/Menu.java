package com.example.cindy.bitswhere;

/**
 * Created by Cindy on 2018-10-30.
 */

public class Menu {
    private String name;
    private int image;
    private int bgc;

    public int getImage() {
        return image;
    }

    public int getBgc(){return bgc;};

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Menu(String name, int image,int bgc){
        this.name = name;
        this.image = image;
        this.bgc = bgc;
    }
}
