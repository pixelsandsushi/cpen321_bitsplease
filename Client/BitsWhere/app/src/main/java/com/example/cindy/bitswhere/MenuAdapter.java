package com.example.cindy.bitswhere;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Cindy on 2018-10-30.
 */

public class MenuAdapter extends BaseAdapter {
    Context context;
    List<Menu> menuList;

    public Context getContext() {
        return context;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public MenuAdapter(Context context, List<Menu> menuList) {
        this.context = context;
        this.menuList = menuList;
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Andale Mono.ttf");
        View v = View.inflate(context, R.layout.listview_menu, null);
        TextView mName = (TextView) v.findViewById(R.id.name);
        ImageView image = (ImageView) v.findViewById(R.id.imageView);
        mName.setText(menuList.get(position).getName());
        mName.setTypeface(tf);
        image.setImageResource(menuList.get(position).getImage());
        v.setBackgroundResource(menuList.get(position).getBgc());

        if (menuList.get(position).getName().length() > 12)
            mName.setTextSize(30);
        return v;
    }
}
