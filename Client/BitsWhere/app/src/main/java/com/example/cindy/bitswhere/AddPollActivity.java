package com.example.cindy.bitswhere;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class AddPollActivity extends AppCompatActivity {

    private EditText newMeetUpTitle;
    private TextView mDisplayStartDate;
    private TextView mDisplayStartTime;
    private DatePickerDialog.OnDateSetListener mStartDateSetListener;
    private TimePickerDialog.OnTimeSetListener mStartTimeSetListener;
    private TextView mDisplayEndDate;
    private TextView mDisplayEndTime;
    private DatePickerDialog.OnDateSetListener mEndDateSetListener;
    private TimePickerDialog.OnTimeSetListener mEndTimeSetListener;
    public  String mid,pid;
    private String startDate, endDate,startTime,endTime;
    private JSONArray attributes = new JSONArray();
    private ItemSelectedAdapter adapter;
    private ListView listView;
    private TextView mPID;
    public String teamID,userID;
    private ImageView logOut;

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button btnCreate;


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_poll);

        newMeetUpTitle = (EditText) findViewById(R.id.editTitle);

        selectDateAndTime();
        Bundle extras = getIntent().getExtras();
        userID = extras.getString("UID");
        teamID = extras.getString("TID");
        mTextView = (TextView) findViewById(R.id.text);
//        mTextView.setText(teamID);
        listView =(ListView) findViewById(R.id.listview);
        setWidget();

        logOut = findViewById(R.id.logout);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(AddPollActivity.this,
                        MainActivity.class);

                myIntent.putExtra("UID",userID);
                startActivity(myIntent);

            }
        });

        btnCreate = (Button) findViewById(R.id.btnCreate);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject json = new JSONObject();

                for(Item hold: adapter.getAllData()){
                    if(hold.isCheckbox()){
                        attributes.put(hold.getName());
                    }
                }

                try {
                    json.put("meetupname", newMeetUpTitle.getText().toString());
                    json.put("startdate",startDate);
                    json.put("starttime",startTime);
                    json.put("enddate",endDate);
                    json.put("endtime",endTime);
//                                                 json.put("attributes",attributes);
                    json.put("TID",teamID);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                RequestQueue queue = Volley.newRequestQueue(AddPollActivity.this);

                String url = "https://bitsplease-meetup-service.herokuapp.com/newMeetup";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, json,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    mid = response.getString("MID");
                                    pid = response.getString("PID");
                                    Intent myIntent = new Intent(AddPollActivity.this,
                                            ViewPollActivity.class);
                                    myIntent.putExtra("midValue",mid);
                                    myIntent.putExtra("pidValue",pid);
                                    myIntent.putExtra("TID",teamID);
                                    myIntent.putExtra("UID",userID);
                                    startActivity(myIntent);
//                                mTextView.setText(pid);

                                } catch (JSONException e) {
                                    Toast.makeText(AddPollActivity.this, "Error getting JSON",Toast.LENGTH_SHORT).show();                                                             }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                                                     mTextView.setText("Error getting response");
                        Toast.makeText(AddPollActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                });
                queue.add(jsonObjectRequest);

            }

        }

        );

    }

    private void selectDateAndTime(){

        selectStart();
        selectEnd();

    }


    private void selectStart(){
        mDisplayStartDate = (TextView) findViewById(R.id.tvStartDate);
        mDisplayStartTime = (TextView) findViewById(R.id.tvStartTime);
        mDisplayStartDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        AddPollActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mStartDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mStartDateSetListener = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day){
                startDate = month + 1 +"/" + day + "/" + year;
                mDisplayStartDate.setText(startDate);
            }
        };
        mDisplayStartTime.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(AddPollActivity.this, mStartTimeSetListener, hour, minute,true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mStartTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mDisplayStartTime.setText(selectedHour + ":" + selectedMinute);
                startTime = selectedHour + ":" + selectedMinute;
            }
        };
    }

    private void selectEnd(){
        mDisplayEndDate = (TextView) findViewById(R.id.tvEndDate);
        mDisplayEndTime = (TextView) findViewById(R.id.tvEndTime);


        mDisplayEndDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        AddPollActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mEndDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mEndDateSetListener = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day){
                endDate = month + 1 +"/" + day + "/" + year;
                mDisplayEndDate.setText(endDate);
            }
        };

        mDisplayEndTime.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(AddPollActivity.this, mEndTimeSetListener, hour, minute,true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mEndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mDisplayEndTime.setText(selectedHour + ":" + selectedMinute);
                endTime = selectedHour + ":" + selectedMinute;
            }
        };

    }



    public void setWidget(){
        ArrayList<Item> itemList;

        //Adding data to ArrayList
        itemList = new ArrayList<Item>();
        itemList.add(new Item("Cafe nearby", false));
        itemList.add(new Item("Silent study", false));
        itemList.add(new Item("Have power outlets", false));
        itemList.add(new Item("Study Room", false));
        itemList.add(new Item("Have whiteboard", false));

        //Setup Adapter for Listview
        adapter = new ItemSelectedAdapter(AddPollActivity.this,itemList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int position, long l) {
                //Item Selected from list
                adapter.setCheckBox(position);

            }
        });


    }




}
