package com.example.cindy.bitswhere;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

public class HomeScreenActivity extends AppCompatActivity
                                implements JoinTeamDialog.OnInputListener,CreateTeamDialog.OnInputListener {
    private static final String TAG = "HomeScreenActivity";

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";


    @Override
    public void sendInput(String input) {
        Log.d(TAG, "sendInput: got the input: " + input);
        mInput = input;
        putJoinRequest();
    }
    @Override
    public void sendNewTeamInput(String input) {
        Log.d(TAG, "sendInput: got the input: " + input);
        mInput = input;
        postNewTeamRequest();
    }

    //widgets
    private Button joinTeamButton,viewTeamButton;
    private Button createTeamButton;
    public TextView mJoinTeamDisplay;
    public TextView mCreateTeamDisplay;
    private String userID, accountName, email;
    private ImageView logOut;
    //vars
    public String mInput;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        joinTeamButton = findViewById(R.id.btnJoinTeam);
        createTeamButton = findViewById(R.id.btnCreateTeam);
        viewTeamButton = findViewById(R.id.btnViewTeam);
        mJoinTeamDisplay = findViewById(R.id.joinTeamInputDisplay);
        mCreateTeamDisplay = findViewById(R.id.createTeamInputDisplay);

        Bundle extras = getIntent().getExtras();
        userID = extras.getString("UID");
//        userID = "108296247272471916735";
//        accountName = extras.getString("displayName");


        logOut = findViewById(R.id.logout);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(HomeScreenActivity.this,
                        MainActivity.class);

                myIntent.putExtra("UID",userID);
                startActivity(myIntent);

            }
        });

        joinTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: opening dialog.");

                FragmentManager fm = getSupportFragmentManager();
                JoinTeamDialog dialog = new JoinTeamDialog();
                dialog.show(fm,"JoinTeamDialog");

            }
        });


        createTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: opening dialog.");

                FragmentManager fm = getSupportFragmentManager();
                CreateTeamDialog dialog = new CreateTeamDialog();
                dialog.show(fm,"CreateTeamDialog");

            }
        });

        viewTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(HomeScreenActivity.this,
                        ViewTeamActivity.class);

                myIntent.putExtra("UID",userID);
                startActivity(myIntent);


            }
        });

//        try{
//            buildCalendar();
//        }catch (IOException e) {
//            e.printStackTrace();
//        }catch (GeneralSecurityException e2){
//            e2.printStackTrace();
//        }

//        Thread thread = new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                try  {
//                    buildCalendar();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        thread.start();



    }

    private void setInputToTextView(){
        mJoinTeamDisplay.setText(mInput);
    }

//    private void setNewTeamInputToTextView(){
//        mCreateTeamDisplay.setText(mInput);
//    }

    private void postNewTeamRequest(){


        RequestQueue Queue = Volley.newRequestQueue(HomeScreenActivity.this);
        String url = "https://bitsplease-team-service.herokuapp.com/newTeam";
        JSONObject newTeamJson = new JSONObject();
        try{
            newTeamJson.put("teamDisplayName",mInput);
            newTeamJson.put("UID",userID);
        }catch (JSONException e) {
            Toast.makeText(this, "Error Getting JSON",Toast.LENGTH_SHORT).show();
        }


        // Request a string response from the provided URL.
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, newTeamJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(HomeScreenActivity.this, "Success creating new team",Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.toString().endsWith("ClientError"))
                    Toast.makeText(HomeScreenActivity.this, "Please input a valid team name",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(HomeScreenActivity.this, "This didn't work",Toast.LENGTH_SHORT).show();

            }
        });
        // Add the request to the RequestQueue.
        Queue.add(jsonRequest);

    }

    private void putJoinRequest(){



            RequestQueue Queue = Volley.newRequestQueue(HomeScreenActivity.this);
            String url = "https://bitsplease-team-service.herokuapp.com/addToTeam";
            JSONObject Json = new JSONObject();
            try {
                Json.put("TID", mInput);
                Json.put("UID", userID);
            } catch (JSONException e) {
                Toast.makeText(this, "Error Getting JSON", Toast.LENGTH_SHORT).show();
            }


            // Request a string response from the provided URL.
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.PUT, url, Json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(HomeScreenActivity.this, "Success joining the team!", Toast.LENGTH_SHORT).show();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.toString().endsWith("ClientError"))
                        Toast.makeText(HomeScreenActivity.this, "Please input a valid team ID",Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(HomeScreenActivity.this, "This Doesn't Work", Toast.LENGTH_SHORT).show();
                }
            });
            // Add the request to the RequestQueue.
            Queue.add(jsonRequest);


    }


//    public void buildCalendar()throws IOException, GeneralSecurityException{
//        // Build a new authorized API client service.
////        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
//        final NetHttpTransport HTTP_TRANSPORT = new com.google.api.client.http.javanet.NetHttpTransport();
//        Credential mCredential = getCredentials(HTTP_TRANSPORT);
//        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, mCredential)
//                .setApplicationName("Calendar")
//                .build();
//
//        // List the next 10 events from the primary calendar.
//        DateTime now = new DateTime(System.currentTimeMillis());
//        Events events = service.events().list("primary")
//                .setMaxResults(10)
//                .setTimeMin(now)
//                .setOrderBy("startTime")
//                .setSingleEvents(true)
//                .execute();
//        List<Event> items = events.getItems();
//        if (items.isEmpty()) {
//            System.out.println("No upcoming events found.");
//        } else {
//            System.out.println("Upcoming events");
//            for (Event event : items) {
//                DateTime start = event.getStart().getDateTime();
//                if (start == null) {
//                    start = event.getStart().getDate();
//                }
//                System.out.printf("%s (%s)\n", event.getSummary(), start);
//            }
//        }
//    }
//
//
//
//    /**
//     * Creates an authorized Credential object.
//     * @param HTTP_TRANSPORT The network HTTP Transport.
//     * @return An authorized Credential object.
//     * @throws IOException If the credentials.json file cannot be found.
//     */
//    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
//        // Load client secrets.
//        ClassLoader HS = HomeScreenActivity.class.getClassLoader();
//
////        InputStream in = HS.getResource(CREDENTIALS_FILE_PATH).openStream();
//        InputStream is = HomeScreenActivity.this.getAssets().open("credentials.json");
//        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(is));
//
//        // Build flow and trigger user authorization request.
//        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
//                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
////                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
//                .setAccessType("offline")
//                .build();
//        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
//        return new AuthorizationCodeInstalledApp(flow, receiver).authorize(userID);
//    }

}


